<?php

return [

    'Jordan'=>'Jordan',
    'Beryanak'=>'Beryanak',
    'Vanak'=>'Vanak',
    'TehranPars'=>'TehranPars',
    'Ekbatan'=>'Ekbatan',
    'YousefAbad'=>'YousefAbad',
    'Qeytarieh'=>'Qeytarieh',
    'Niavaran'=>'Niavaran',
    'SaadatAbad'=>'SaadatAbad',
    'ShahrakeGharb'=>'ShahrakeGharb',
    'KarimKhan'=>'KarimKhan',
    'Gisha'=>'Gisha',
    'Velenjak'=>'Velenjak',
    'EnghelabSquare'=>'EnghelabSquare',
    'Elahiyeh'=>'Elahiyeh',
    'MehrabadInternationalAirport'=>'MehrabadInternationalAirport',
    'ImamKhomeiniInternationalAirport'=>'ImamKhomeiniInternationalAirport',
    'BagheFeyz'=>'BagheFeyz',
    'Afsariyeh'=>'Afsariyeh',
    'TehranBazaar'=>'TehranBazaar',
    'AmirAbad'=>'AmirAbad',
    'Baharestan'=>'Baharestan',
    'Piroozi'=>'Piroozi',
    'Argentin'=>'Argentin',

];
