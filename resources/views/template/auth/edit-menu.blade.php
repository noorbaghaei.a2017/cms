@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">

  @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">

                            @include('template.alert.error')
                            @include('template.alert.success')

                               <!-- profile-edit-container--> 
                               <div class="profile-edit-container fl-wrap block_box">
                                <form method="POST" action="{{route('update.menu.job',['token'=>$item->token])}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="custom-form" >
                                        <div class="row">
                                         <!--col --> 
                                         <div class="col-md-4">
                                               
                                               
                                                   
                                         <label> {{__('cms.title')}} <i class="fal fa-user"></i></label>
                                        <input type="text" name="title" placeholder="" value="{{$item->title}}"/>
                                        
  
                                               
                                           </div>
                                           <!--col end--> 
                                           
                                                      <!--col --> 
                                         <div class="col-md-4">
                                               
                                               
                                                   
                                               <label> {{__('cms.price')}} <i class="fas fa-euro-sign"></i></label>
                                              <input type="text" name="price" placeholder="" value="{{$item->price}}"/>
                                              
      
                                                            
      
      
                                                       
                                                     
                                                 </div>
                                                 <!--col end--> 
                                            <!--col --> 
                                            <div class="col-md-4">
                                               
                                                <div class="add-list-media-wrap">
                                                   
                                                @if(!$item->Hasmedia('images'))
                                                        <div id="bg" class="fuzone">
                                                        
                                                                <div id="text-image" class="fu-text">
                                                                 <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                                                            </div>
                                                               @else
                                                               <div id="bg" class="fuzone" style="background-image:url({{$item->getFirstMediaUrl('images')}});background-size:cover;">
                                                               
                                                               
                                                           
                                                               @endif    
                                                            
                                                               
                                                                <input type="file" name="image" class="upload" onchange="loadFile(event)">
                                                       

                                                        </div>


                                                  
                                                </div>
                                            </div>
                                            <!--col end--> 
                                            
                                           
                                                                                          
                                        </div>
                                        <div class="row">
                                    <!--col --> 
                                    <div class="col-md-12">
                                               
                                               
                                                   
                                               <label> {{__('cms.excerpt')}} <i class="fal fa-text"></i></label>
                                              <input type="text" name="excerpt" placeholder="" value=""/>
                                              
      
                                                            
      
      
                                                       
                                                     
                                                 </div>
                                                 <!--col end--> 
                                        </div>
                                    </div>
                                    <button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>

                                    </form>

                                </div>
                                <!-- profile-edit-container end-->

                                                  
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

function loadFile(e) {
	 image = $('#bg');
     text = $('#text-image');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




