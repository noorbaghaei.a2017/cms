
@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  <input id="lang_form" type="text" value="{{LaravelLocalization::getCurrentLocale()}}" hidden>
  @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
  

                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">

                                @include('template.alert.error')
                                
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">

                                        <div class="row">
                                            <div class="col-sm-6" >
                                               <div style="background: #4DB7FE !important;padding: 14px 20px;">
                                                <a class="translate_buttom btn btn-primary"    href="{{ route('client.employee.create.werbung') }}">{{ __('cms.employee') }}</a>
                                               </div>

                                            </div>
                                            <div class="col-sm-6" >
                                                <div style="background: #4DB7FE !important;padding: 14px 20px;">
                                                    <a class="translate_buttom btn btn-primary"    href="{{ route('client.employer.create.werbung') }}">{{ __('cms.employer') }}</a>
                                                   </div>

                                            </div>
                                        </div>

               

</div>

</div>



                    























                               
                              
                                 
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>

                    

                      
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')




<script>


$('input[name=price]').keyup(function(){
   
   $(this).val($(this).val().replace(/[^\d]/,''));
});




$('#showCheckoutHistory').change(function() {
        if ($(this).prop('checked')) {
            alert("You have elected to show your checkout history."); //checked
        }
        else {
            alert("You have elected to turn off checkout history."); //not checked
        }
    });

    function changeStatusBanner(e) {

        $('#banner_info').hide();
        $('#slide_info').hide();
        $('#video_info').hide();
      $target="#"+e.target.dataset.info;
      $($target).show();
	 
};

function loadFile(e) {
	 image = $('#bg');
     text = $('#text-image');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat');
    
};

function loadSlide1(e) {
	 image = $('#slide_1');
     text = $('#text-slide-1');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};
function loadSlide2(e) {
	 image = $('#slide_2');
     text = $('#text-slide-2');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};
function loadSlide3(e) {
	 image = $('#slide_3');
     text = $('#text-slide-3');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};

function clearFileBanner(){
    image = $('#banner');
     text = $('#text-banner');
    text.show();
    
    image.attr('style','background-image:url("")');
    
    $banner=$('input[name="banner"]');
    $banner.val('');
   
}
function clearFileImage(){
    image = $('#bg');
     text = $('#text-image');
    text.show();
    
    image.attr('style','background-image:url("")');
    
    $banner=$('input[name="image"]');
    $banner.val('');
   
}
function clearFileSlide1(){
    image = $('#slide_1');
     text = $('#text-slide-1');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="slide_1"]');
    $banner.val('');
   
}

function clearFileSlide2(){
    image = $('#slide_2');
     text = $('#text-slide-2');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="slide_2"]');
    $banner.val('');
   
}
function clearFileSlide3(){
    image = $('#slide_3');
     text = $('#text-slide-3');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="slide_3"]');
    $banner.val('');
   
}
function clearFileGallery1(){
    image = $('#photo_1');
     text = $('#text-photo-1');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="photo_1"]');
    $banner.val('');
   
}
function clearFileGallery2(){
    image = $('#photo_2');
     text = $('#text-photo-2');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="photo_2"]');
    $banner.val('');
   
}
function clearFileGallery3(){
    image = $('#photo_3');
     text = $('#text-photo-3');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="photo_3"]');
    $banner.val('');
   
}



function loadPhoto1(e) {
	 image = $('#photo_1');
     text = $('#text-photo-1');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};
function loadPhoto2(e) {
	 image = $('#photo_2');
     text = $('#text-photo-2');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};
function loadPhoto3(e) {
	 image = $('#photo_3');
     text = $('#text-photo-3');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};

function loadBanner(e) {
	 image = $('#banner');
     text = $('#text-banner');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};



function loadSubCategory(e) {


     
$category= e.target.value;
$lang=$("html").attr("lang");




$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/sub/category/product/'+$category+"/"+$lang,
    data: { field1:$category,field2:$lang} ,
    beforeSend:function(){
    
},
beforeSend:function(){
    $('#sub_category + .nice-select').attr('style','opacity:0');
},

success: function (response) {


   if(response.data.status){

       console.log(response.data);

       $('#sub_category').empty();
     
       $('#sub_category + .nice-select .list').empty();

       $('#sub_category + .nice-select > span.current').text('');

       $('#sub_category + .nice-select .list').empty();
       
     
     //start for each for load select input 

       $.each(response.data.result, function(key, index) {
   
                        if(key==0){
                                $('#sub_category').append("<option value='"+index.id+"'   selected>"+index.translate+"</option>");
                                $('#sub_category + .nice-select > span.current').text(index.translate);
                            }
                            else{
                                $('#sub_category').append("<option value='"+index.id+"'   >"+index.translate+"</option>");
                                $('#sub_category + .nice-select  ul.list ').append("<li class='option'  data-value='"+index.id+"'   >"+index.translate+"</li>"); 
                            }
         
 
        });

       }


  //end for each for load select input 

   
},
error: function (xhr,ajaxOptions,thrownError) {

console.log(xhr,ajaxOptions,thrownError);

    $("#sub_category").empty();
    $('#sub_category + .nice-select .list').empty();
    $('#sub_category + .nice-select > span.current').text('');
    $('#sub_category + .nice-select').attr('style','opacity:0');

  
},
complete:function(){
    $('#sub_category + .nice-select').attr('style','opacity:1');
}

});
}

function loadCity(e) {

var myInput = document.getElementById("postal_code");
     


if(myInput.value.length>=5){



console.log($('#latitude').val());
          

    $code=$("#postal_code").val();
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
        $('#city + .nice-select').attr('style','opacity:0');
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
           $('#city').empty();
           $('#city + .nice-select .list').empty();
           $('#city + .nice-select > span.current').text('');
         
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
           
           
            $('#city + .nice-select .list').empty();
                $('#city + .nice-select .list').append("<li data-value='"+response.data.result[0].fields.plz_name+"' class='option  selected focus'>"+response.data.result[0].fields.plz_name+"</li>");
           
                $('#city + .nice-select > span.current').text(response.data.result[0].fields.plz_name);

              

       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
    $('#city + .nice-select > span.current').text('');
    $('#city + .nice-select').attr('style','opacity:0');

    },
    complete:function(){
        $('#city + .nice-select').attr('style','opacity:1');
}

});

}
}

</script>


@endsection




