@extends('template.app')


@section('content')

 <!-- content-->
 <div class="content">
 
 @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                        @include('template.auth.menu')
                           <!-- dashboard content-->
                           
                               
                                    <div class="col-md-9">
                                  
                                        @include('template.alert.error')
                                        @include('template.alert.success')
                                        @include('template.auth.imports.bussiness')

                                        @include('template.auth.imports.product')

                                        @include('template.auth.imports.werbung')





                                    </div>
                                   
                             
                           
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->



@endsection


@section('heads')


@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif

@endsection


