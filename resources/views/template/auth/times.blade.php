@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">

  @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                            @include('template.alert.error')
                                 <!-- profile-edit-container--> 
                                 <div class="profile-edit-container fl-wrap block_box">
                                    <form class="custom-form" action="{{route('job.change.time',['token'=>$item->token])}}" method="POST">
                                    @csrf
                                        <label> {{__('cms.monday')}} <i class="fal fa-clock"></i></label>
                                        <input type="text" name="monday" placeholder="for example : 9AM-5PM" value="{{is_null($item->time) ? '' : $item->time->monday}}"/>
                                        <label>{{__('cms.tuesday')}}<i class="fal fa-clock"></i>  </label>
                                        <input type="text" name="tuesday" placeholder="for example : 9AM-5PM" value="{{is_null($item->time) ? '' : $item->time->tuesday}}"/>
                                        <label> {{__('cms.wednesday')}} <i class="fal fa-clock"></i>  </label>
                                        <input type="text" name="wednesday" placeholder="for example : 9AM-5PM" value="{{is_null($item->time) ? '' : $item->time->wednesday}}"/>
                                        <label> {{__('cms.thursday')}} <i class="fal fa-clock"></i>  </label>
                                        <input type="text" name="thursday" placeholder="for example : 9AM-5PM" value="{{is_null($item->time) ? '' : $item->time->thursday}}"/>
                                        <label> {{__('cms.friday')}} <i class="fal fa-clock"></i>  </label>
                                        <input type="text" name="friday" placeholder="for example : 9AM-5PM" value="{{is_null($item->time) ? '' : $item->time->friday}}"/>
                                        <label> {{__('cms.saturday')}} <i class="fal fa-clock"></i>  </label>
                                        <input type="text" name="saturday" placeholder="for example : 9AM-5PM" value="{{is_null($item->time) ? '' : $item->time->saturday}}"/>
                                        <label> {{__('cms.sunday')}} <i class="fal fa-clock"></i>  </label>
                                        <input type="text" name="sunday" placeholder="for example : 9AM-5PM" value="{{is_null($item->time) ? '' : $item->time->sunday}}"/>
                                       
                                        <button class="btn    color2-bg  float-btn">{{__('cms.update')}} <i class="fal fa-save"></i></button>
                                    </form>
                                </div>
                                <!-- profile-edit-container end-->                    
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




