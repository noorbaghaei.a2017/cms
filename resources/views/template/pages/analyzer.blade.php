@extends('template.app')

@section('content')


    <!-- Page Title -->
    <section class="page-title">
        <div class="outer-container">
            <div class="image">
                <img src="{{asset('template/images/background/18.jpg')}}" alt="" />
            </div>
        </div>
    </section>
    <!-- End Page Title -->

    <!-- Page Breadcrumb -->
    <section class="page-breadcrumb">
        <div class="image-layer" style="background-image:url({{asset('template/images/background/1.png')}})"></div>
        <div class="container">
            <div class="clearfix">
                <div class="pull-right">

                </div>
                <div class="pull-left">

                </div>
            </div>
        </div>
    </section>
    <!-- End Page Breadcrumb -->

    <!-- Doctor Single Section -->
    <section class="doctor-detail-section">
        <div class="container">
            <div class="row">



                <!-- Content Column -->
                <div class="content-column col-lg-7 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <h2>اطلاعات شخصی</h2>
                        <ul class="doctor-info-list">
                            <li><span>نام </span> امین</li>
                            <li><span>نام خانوادگی</span>نوربقایی</li>
                            <li><span>سن</span>30</li>
                        </ul>
                    </div>
                </div>
                <!-- Image Column -->
                <div class="image-column col-lg-5 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <div class="image">
                            <video  id="video"></video>
{{--                            <img src="{{asset('template/images/resource/doctor.jpg')}}" alt="" />--}}
                            <div class="number-box">
                                <a href="tel:09195995044" class="play-button" ><i class="ripple"></i><i class="icon flaticon-video"></i></a>
                               09195995044
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Page Section -->

{{--    <div class="both">--}}
{{--        <video  id="video"></video>--}}
{{--        <canvas id="can"></canvas>--}}

{{--    </div>--}}



@endsection


@section('scripts')

    <script>


        var video = document.getElementById('video');

        if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                video.srcObject = stream;
                video.play();
            });
        }




    </script>

@endsection

@section('heads')


    <style>
        #video{
            width: 100%;
        }

        #can{
            width: 900px;
            height: 900px;
        }

    </style>

@endsection
