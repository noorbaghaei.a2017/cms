@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">
                    
                    <!-- scroll-nav-wrapper--> 
                    <div class="scroll-nav-wrapper fl-wrap">
                        <div class="container">
                            <nav class="scroll-nav scroll-init">
                                <ul class="no-list-style">
                                    <li><a class="act-scrlink" href="#sec1"><i class="fal fa-images"></i> {{__('cms.detail')}}</a></li>
                                    <li><a href="#sec3"><i class="fal fa-calendar-check"></i>{{__('cms.questions')}}</a></li>
                                   
                                </ul>
                            </nav>
                           
                        </div>
                    </div>
                    <!-- scroll-nav-wrapper end--> 
                    <section class="gray-bg no-top-padding">
                        <div class="container">
                           
                            <div class="clearfix"></div>
                            <div class="row">
                                <!-- list-single-main-wrapper-col -->
                                <div class="col-md-8">
                                    <!-- list-single-main-wrapper -->
                                    <div class="list-single-main-wrapper fl-wrap"  data-scrollax-parent="true" id="sec1">
                                        <div class="list-single-main-media fl-wrap">
                                            <div class="single-slider-wrap">
                                            @if(count($medias) > 0)


                                            <div class="single-slider fl-wrap">
                                                    <div class="swiper-container">
                                                        <div class="swiper-wrapper lightgallery">
                                                        @foreach($medias as $media)
                                                            <div class="swiper-slide hov_zoom"><img src="/media/{{$media->id}}/{{$media->file_name}}" alt=""><a href="/media/{{$media->id}}/{{$media->file_name}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a></div>
                                                      
                                                      @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="listing-carousel_pagination">
                                                    <div class="listing-carousel_pagination-wrap">
                                                        <div class="ss-slider-pagination"></div>
                                                    </div>
                                                </div>
                                                <div class="ss-slider-cont ss-slider-cont-prev color2-bg"><i class="fal fa-long-arrow-left"></i></div>
                                                <div class="ss-slider-cont ss-slider-cont-next color2-bg"><i class="fal fa-long-arrow-right"></i></div>
                                           
                                           

                                            @else



                                            @if($item->Hasmedia('images'))
                                                                   <img  src="{{$item->getFirstMediaUrl('images')}}" alt="{{ $item->title }}" title="{{ $item->title }}" >

           
                                                                    @else
                                                                    <img  src="{{asset('template/images/no-image.jpg')}}" alt="{{ $item->title }}" title="{{ $item->title }}" >

                                               
                                           @endif
                                           @endif
                                            </div>
                                        </div>
                                        <!-- list-single-main-item --> 
                                        <div class="list-single-main-item fl-wrap block_box">
                                            <div class="list-single-main-item-title">
                                                <h3>{{__('cms.description')}}</h3>
                                            </div>
                                            <div class="list-single-main-item_content fl-wrap">
                                                <p>{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'text')}}</p>
                                               
                                            </div>
                                        </div>
                                        <!-- list-single-main-item end -->                                               
                                                                         
                                        <!-- accordion-->
                                        <div class="accordion mar-top" id="sec3">
                                        @foreach($questions as $question)
                                           
                                           @if($loop->first)
                                           <a class="toggle act-accordion" href="#">{{convert_lang($question,LaravelLocalization::getCurrentLocale(),'title')}}<span></span></a>
                                            <div class="accordion-inner visible">
                                                <h4 class="simple-title">{{convert_lang($question,LaravelLocalization::getCurrentLocale(),'title')}} </h4>
                                                <p>{{convert_lang($question,LaravelLocalization::getCurrentLocale(),'answer')}}</p>
                                            </div>

                                           @else
                                           <a class="toggle act-accordion" href="#">{{convert_lang($question,LaravelLocalization::getCurrentLocale(),'title')}}<span></span></a>
                                            <div class="accordion-inner ">
                                                <h4 class="simple-title">{{convert_lang($question,LaravelLocalization::getCurrentLocale(),'title')}} </h4>
                                                <p>{{convert_lang($question,LaravelLocalization::getCurrentLocale(),'answer')}}</p>
                                            </div>
                                           @endif
                                            @endforeach
                                       
                                          
                                        </div>
                                        <!-- accordion end -->       
                                                                    
                                                                           
                                                                        
                                    </div>
                                </div>
                                <!-- list-single-main-wrapper-col end -->
                                <!-- list-single-sidebar -->
                                <div class="col-md-4">
                                @include('template.alert.success')
                                @include('template.alert.error')
                                @if(auth('client')->check())
                                @if(auth('client')->user()->id!=$item->client)
                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap block_box">
                                        <div class="box-widget-item-header">
                                            <h3>{{__('cms.cv')}}</h3>
                                        </div>
                                        <div class="box-widget">
                                            <div class="box-widget-content">
                                                <form action="{{route('send.cv',['token'=>$item->token])}}" method="POST"   class="add-comment custom-form" enctype="multipart/form-data">
                                                    @csrf
                                                    <fieldset>
                                                       
                                                       
                                                        <input type="file" name="document" placeholder="{{__('cms.document')}}" value=""/>
                                                        <div class="clearfix"></div>
                                                        
                                                      
                                                    </fieldset>
                                                    <button type="submit" class="btn color2-bg  float-btn" >{{__('cms.send')}} <i class="fal fa-bookmark"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end --> 
                                    @endif 
                                    @else
                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap block_box">
                                                                            
                                                                            <div class="box-widget">
                                                                         
                                                                <!-- listsearch-input-item-->
                                                            <div onclick="modalOpen(event)" class="listsearch-input-item" style="margin-bottom:0">
                                                                <button  class="header-search-button color-bg" style="border-radius:unset"><span>{{__('cms.login')}}</span></button>
                                                            </div>
                                                            <!-- listsearch-input-item end-->    
                                    </div>
                                                                        </div>
                                                                        <!--box-widget-item end -->  
                                    @endif                                                                 
                                    <!--box-widget-item -->                                       
                                    <div class="box-widget-item fl-wrap block_box">
                                        <div class="box-widget-item-header">
                                            <h3>{{__('cms.location')}} / {{__('cms.contacts')}}  </h3>
                                        </div>
                                        <div class="box-widget">
                                            <div class="map-container">
                                            @if($item->Hasmedia('images'))
                                                                   <img width="100%"  src="{{$item->getFirstMediaUrl('images')}}" alt="{{ $item->title }}" title="{{ $item->title }}" >

           
                                                                    @else
                                                                    <img width="100%"  src="{{asset('template/images/no-image.jpg')}}" alt="{{ $item->title }}" title="{{ $item->title }}" >

                                                             

                                                               @endif
                                            </div>
                                            <div class="box-widget-content bwc-nopad">
                                                <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                                    <ul class="no-list-style">
                                                        @if($item->access_address=="1")
                                                      <li><span><i class="fal fa-map-marker"></i> {{__('cms.address')}} :</span> <a target='_blank' href="https://maps.google.com/?q={{$item->address}}">{{$item->address}}, {{$item->postal_code}} {{$item->city}}</a></li>
                                                     @endif
                                                      @if($item->access_mobile=="1")
                                                      <li><span><i class="fal fa-phone"></i> {{__('cms.mobile')}} :</span> <a href="tel:{{$item->mobile}}">{{$item->mobile}}</a></li>
                                                      @endif
                                                      @if($item->access_email=="1")
                                                        <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} :</span> <a href="mailto:{{$item->email}}">{{$item->email}}</a></li>
                                                   @endif
                                                    </ul>
                                                </div>
                                                @if($item->info->instagram || $item->info->whatsapp || $item->info->facebook || $item->info->website)
                                                <div class="list-widget-social bottom-bcw-box  fl-wrap">
                                                    <ul class="no-list-style">
                                                        @if($item->info->instagram)
                                                        <li><a href="https://www.instagram.com/{{$item->info->instagram}}" target="_blank" ><i class="fab fa-instagram"></i></a></li>

                                                        @endif

                                                        @if($item->info->whatsapp)
                                                        <li><a href="https://wa.me/{{$item->info->whatsapp}}" target="_blank"><i class="fab fa-whatsapp"></i></a></li>

                                                        @endif

                                                        @if($item->info->facebook)
                                                        <li><a href="{{$item->info->facebook}}" target="_blank" ><i class="fab fa-facebook"></i></a></li>

                                                        @endif

                                                        @if($item->info->telegram)
                                                        <li><a href="https://telegram.me/{{$item->info->telegram}}" target="_blank" ><i class="fab fa-telegram"></i></a></li>

                                                        @endif

                                                        

                                                        @if($item->info->website)
                                                        <li><a href="{{$item->info->website}}" target="_blank" ><i class="fab fa-chrome"></i></a></li>

                                                        @endif
                                                    </ul>
                                                </div>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->                                  
                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap block_box">
                                        <div class="box-widget-item-header">
                                            <h3> {{__('cms.salary')}} </h3>
                                        </div>
                                        <div class="box-widget">
                                            <div class="box-widget-content">
                                                <div class="claim-price-wdget fl-wrap">
                                                    <div class="claim-price-wdget-content fl-wrap">
                                                        <div class="pricerange fl-wrap"><span>{{__('cms.salary')}} : </span> € {{(is_null($item->salary) || empty($item->salary)) ? '0' : $item->salary}}  {{ __('cms.'.$item->type_salary) }} </div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->                               
                                   
                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap block_box">
                                        <div class="box-widget-item-header">
                                            <h3>{{__('cms.popular-werbungs')}}</h3>
                                        </div>
                                        <div class="box-widget  fl-wrap">
                                            <div class="box-widget-content">
                                                <!--widget-posts-->
                                                <div class="widget-posts  fl-wrap">
                                                    <ul class="no-list-style">
                                                    @foreach($popular_items as $item)
                                                        <li>
                                                            <div class="widget-posts-img"><a href="{{route('werbungs.single',['werbung'=>$item->slug])}}">
                                                            @if($item->Hasmedia('images'))
                                                                   <img  src="{{$item->getFirstMediaUrl('images')}}" alt="{{ $item->title }}" title="{{ $item->title }}" >

           
                                                                    @else
                                                                    <img  src="{{asset('template/images/no-image.jpg')}}" alt="{{ $item->title }}" title="{{ $item->title }}" >

                                                             

                                                               @endif
                                                            </a>  
                                                            </div>
                                                            <div class="widget-posts-descr">
                                                                <h4><a href="{{route('werbungs.single',['werbung'=>$item->slug])}}">{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}</a></h4>
                                                             @if($item->access_address=="1")
                                                                <div class="geodir-category-location fl-wrap"><a target='_blank' href="https://maps.google.com/?q={{$item->address}}"><i class="fas fa-map-marker-alt"></i> {{$item->address}}, {{$item->postal_code}} {{$item->city}}</a></div>
                                                             @endif
                                                            </div>
                                                        </li>
                                                    @endforeach    
                                                       
                                                    </ul>
                                                </div>
                                                <!-- widget-posts end-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->      
                                        
                                </div>
                                <!-- list-single-sidebar end -->                                
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->

                @endsection


@section('scripts')
@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<script src="{{asset('template/js/shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/shop.css?')}}{{uniqid()}}">

@else

<script src="{{asset('template/js/ltr-shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-shop.css?')}}{{uniqid()}}">


@endif
<script>

function modalOpen(e){

 
e.preventDefault();
$('.modal , .reg-overlay').fadeIn(200);
$(".modal_main").addClass("vis_mr");
$("html, body").addClass("hid-body");


}

</script>

@endsection