<!-- Start FAQ Area -->
<section class="faq-area">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-lg-6 col-md-12 p-0">
                <div class="faq-image">
                    <img src="{{asset('template/img/sepehri/sepehri.jpeg')}}" alt="image">
                </div>
            </div>

            <div class="col-lg-6 col-md-12 p-0">
                <div class="faq-accordion">
                    <span class="sub-title">سوالات متداول</span>
                    <h2>در صورت تمایل پاسخ سوالات خود را دریافت کنید</h2>

                    <ul class="accordion">

                        @foreach($questions as $question)

                           @if($loop->first)
                                <li class="accordion-item">
                                    <a class="accordion-title active" href="javascript:void(0)">
                                        <i class="fas fa-plus"></i>
                                        {{$question->title}}
                                    </a>

                                    <p class="accordion-content show">{{$question->answer}}</p>
                                </li>
                            @else
                                <li class="accordion-item">
                                    <a class="accordion-title" href="javascript:void(0)">
                                        <i class="fas fa-plus"></i>
                                        {{$question->title}}
                                    </a>

                                    <p class="accordion-content">{{$question->answer}}</p>
                                </li>

                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End FAQ Area -->
