<!--feauture start-->

<section class="grey-bg pattern feuture-bottom white-overlay" data-bg-img="{{asset('template/images/pattern/01.png')}}" data-overlay="3">
    <div class="container">
        <div class="row no-gutters advantage-content">
            @foreach($advantages as $advantage)
            <div class="col-lg-3 col-md-6">
                <div class="featured-item bottom-icon">
                    <div class="featured-title text-uppercase">
                        <h5>{{convert_lang($advantage,LaravelLocalization::getCurrentLocale(),'title')}}</h5>
                    </div>
                    <div class="featured-icon"> <i class="{{$advantage->icon}}"></i>
                    </div> <span><i class="{{$advantage->icon}}"></i></span>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>

<!--feauture end-->
