<!-- Start upcomming Area -->
<section class="junior__upcomming__area section-padding--lg bg--white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center">
                    <h2 class="title__line">امکانات ویژه به زودی</h2>
                    <p>
                        امکانات ویژه ما را شما انتخاب میکنید
                    </p>
                </div>
            </div>
        </div>
        <div class="row upcomming__wrap mt--40">
            <!-- Start Single Upcomming Event -->
            <div class="col-lg-6 col-md-12 col-sm-12 wow fadeInLeft">
                <div class="upcomming__event">
                    <div class="upcomming__thumb">
                        <img src="{{asset('template/images/upcomming/1.png')}}" alt="upcomming images">
                    </div>
                    <div class="upcomming__inner">
                        <h6><a href="#">امکانات ویژه اول</a></h6>
                        <p>
                            توضیحات امکانات اول
                        </p>
                        <ul class="event__time">
                            <li>دانشگاه شریف<i class="fa fa-home"></i></li>
                            <li>۸ تا ۱۰ صبح<i class="fa fa-clock-o"></i></li>
                            <li>چهارشنبه - ۵ شهریور ۱۳۹۹<i class="fa fa-calendar"></i></li>
                        </ul>
                    </div>
                    <div class="event__occur">
                        <img src="{{asset('template/images/upcomming/shape/1.png')}}" alt="shape images">
                        <div class="enent__pub">
                            <span class="time">21st </span>
                            <span class="bate">Dec,2017</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Upcomming Event -->
            <!-- Start Single Upcomming Event -->
            <div class="col-lg-6 col-md-12 col-sm-12 wow fadeInRight">
                <div class="upcomming__event">
                    <div class="upcomming__thumb">
                        <img src="{{asset('template/images/upcomming/2.png')}}" alt="upcomming images">
                    </div>
                    <div class="upcomming__inner">
                        <h6><a href="#">امکانات ویژه دوم</a></h6>
                        <p>
                            توضیحات امکانات دوم
                        </p>
                        <ul class="event__time">
                            <li>دانشگاه شریف<i class="fa fa-home"></i></li>
                            <li>۸ تا ۱۰ صبح<i class="fa fa-clock-o"></i></li>
                            <li>چهارشنبه - ۵ شهریور ۱۳۹۹<i class="fa fa-calendar"></i></li>
                        </ul>
                    </div>
                    <div class="event__occur">
                        <img src="{{asset('template/images/upcomming/shape/1.png')}}" alt="shape images">
                        <div class="enent__pub">
                            <span class="time">21st </span>
                            <span class="bate">Dec,2017</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Upcomming Event -->
        </div>
    </div>
</section>
<!-- End upcomming Area -->


<div class="listing-carousel-wrap fl-wrap" id="sec1">
                        <div class="listing-carousel fl-wrap full-height lightgallery">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">

                                @if(count($slides) > 0)

                                @foreach ($slides as $slide)
                                       <!-- swiper-slide-->
                                       <div class="swiper-slide hov_zoom">
                                        <img  src="/media/{{$slide->id}}/{{$slide->file_name}}"   alt="">
                                        <a href="/media/{{$slide->id}}/{{$slide->file_name}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>
                                         </div>
                                    <!-- swiper-slide end-->
                                @endforeach
                                 
                                 
                                   

                                @else
                                @if(count(showServiceCategory($item->service)->getMedia('gallery')) > 0)
                                @foreach (showServiceCategory($item->service)->getMedia('gallery') as $media)
                                    <!-- swiper-slide-->
                                    <div class="swiper-slide hov_zoom">
                                        <img  src="/media/{{$media->id}}/{{$media->file_name}}"   alt="">
                                        <a href="/media/{{$media->id}}/{{$media->file_name}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>
                                         </div>
                                    <!-- swiper-slide end-->
                                    @endforeach
                                    
                                    @else

                                    <!-- swiper-slide-->
                                    <div class="swiper-slide hov_zoom">
                                        <img  src="{{asset('template/images/slide-defalt-job.jpg')}}"   alt="">
                                        <a href="{{asset('template/images/slide-defalt-job.jpg')}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>
                                         </div>
                                    <!-- swiper-slide end-->
                                     <!-- swiper-slide-->
                                     <div class="swiper-slide hov_zoom">
                                        <img  src="{{asset('template/images/slide-defalt-job.jpg')}}"   alt="">
                                        <a href="{{asset('template/images/slide-defalt-job.jpg')}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>
                                         </div>
                                    <!-- swiper-slide end-->
                                     <!-- swiper-slide-->
                                     <div class="swiper-slide hov_zoom">
                                        <img  src="{{asset('template/images/slide-defalt-job.jpg')}}"   alt="">
                                        <a href="{{asset('template/images/slide-defalt-job.jpg')}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>
                                         </div>
                                    <!-- swiper-slide end-->

                                    @endif
                                  
                                @endif
                                  
                                   
                                   
                                </div>
                            </div>
                        </div>
                        <div class="listing-carousel_pagination">
                            <div class="listing-carousel_pagination-wrap"></div>
                        </div>
                        <div class="listing-carousel-button listing-carousel-button-next"><i class="fas fa-caret-right"></i></div>
                        <div class="listing-carousel-button listing-carousel-button-prev"><i class="fas fa-caret-left"></i></div>
                    </div>