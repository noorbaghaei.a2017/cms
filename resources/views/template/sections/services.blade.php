<!--service start-->

<section  class="dark-bg text-center">
    <div  class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 ml-auto mr-auto">
                <div class="section-title">
                    <h2 class="title"> {{__('cms.services')}}</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel owl-theme" data-items="3" data-md-items="2" data-sm-items="1" data-xs-items="1" data-margin="30">
                    @foreach($services as $service)
                    <div class="item">
                        <div class="service-item">
                            <div class="service-images">
                                @if(!$service->Hasmedia('images'))
                                    <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="{{$service->title}}" title="{{$service->title}}">
                                @else
                                    <img class="img-fluid" src="{{$service->getFirstMediaUrl('images')}}" alt="{{$service->title}}" title="{{$service->title}}">
                                @endif

                                <div class="service-icon"> <i class="{{$service->icon}}"></i>
                                </div>
                            </div>
                            <div class="service-description">
                                <h4>{{$service->title}}</h4>
                            </div>
                        </div>
                    </div>
                        @endforeach

                </div>
            </div>
        </div>
    </div>
</section>

<!--service end-->

