@if ($errors->any())

  <!-- dashboard-list-box--> 
  <div class="dashboard-list-box  fl-wrap">
  @foreach ($errors->all() as $error)
                                    <!-- dashboard-list end-->    
                                    <div class="dashboard-list fl-wrap">
                                        <div class="dashboard-message">
                                            <div class="dashboard-message-text">
                                                <i class="fal fa-times red-bg"></i> 
                                                <p> {{ $error }} </p>
                                            </div>
                                            <div class="dashboard-message-time"></div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->    
                                                                      
    @endforeach                           
                                                                        
  </div>
<!-- dashboard-list-box end--> 

@endif


@if(session()->has('error'))
     <!-- dashboard-list-box--> 
  <div class="dashboard-list-box  fl-wrap">
  
                                <!-- dashboard-list end-->    
                                <div class="dashboard-list fl-wrap">
                                        <div class="dashboard-message">
                                            <div class="dashboard-message-text">
                                                <i class="fal fa-times red-bg"></i> 
                                                <p>  {{session()->get('error')}} </p>
                                            </div>
                                            <div class="dashboard-message-time"></div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->  
                                    </div>  
       
  
@endif
