
@extends('template.app')

@section('content')


  <!-- content-->
  <div class="content">
                               
                    <!--  section  -->
                    <section   id="sec1" data-scrollax-parent="true">
                        <div class="container">
                            <!--about-wrap -->
                            <div class="about-wrap">
                                <div class="row">
                                    <div class="col-md-12">
                                     
                                        <!--box-widget-item -->                                       
                                        <div class="box-widget-item fl-wrap block_box">
                                            <div class="box-widget text-center">
                                               
                                                @if($status)
                                                <h2 style="color:green;padding:15px;">{{__('cms.msg_success_register_server')}}</h2>
                                                
                                                @else
                                                <h2 style="color:red;padding:15px;"> {{__('cms.msg_error_server')}}</h2>

                                               
                                                @endif
    
                                            </div>
                                        </div>
                                        <!--box-widget-item end --> 
                                        @if($status) 
                                                    <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap" style="margin-top:20px;">
                                            <div class="banner-wdget fl-wrap">
                                                <div class="overlay op4"></div>
                                                <div class="bg"  data-bg="{{asset('template/images/welcome-registere.jpeg')}}"></div>
                                                <div class="banner-wdget-content fl-wrap">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->    
                                        @endif                             
                                    </div>
                                  
                                </div>
                            </div>
                            <!-- about-wrap end  --> 
                        </div>
                    </section>
                    <section class="no-padding-section">
                        <div class="map-container">

                            {!! $setting->google_map !!}
                            
                        </div>
                    </section>
                </div>
                <!--content end-->





@endsection




