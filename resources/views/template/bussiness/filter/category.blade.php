
                                        <div class="tab">
                                            <div id="category-search" class="tab-content">
                                                <!-- category-carousel-wrap -->  
                                                <div class="category-carousel-wrap fl-wrap">
                                                    <div class="category-carousel fl-wrap full-height">
                                                        <div class="swiper-container">
                                                            <div class="swiper-wrapper">

                                                            @foreach ($more_view_category_bussiness as $category)
                                                                
                                                             <!-- category-carousel-item -->  
                                                             <div class="swiper-slide">
                                                                    <a href="{{route('search.page.bussiness',['filter'=>1,'category'=>$category->id])}}" class="category-carousel-item fl-wrap full-height" >
                                                                        @if($category->Hasmedia('images'))

                                                                       <img src="{{$category->getFirstMediaUrl('images')}}" alt="">

                                                                       @else
                                                                       <img src="{{asset('template/images/no-image.jpg')}}" alt="">

                                                                       @endif
                                                                        <div class="category-carousel-item-icon " style="background-color:{{findOriginCategoryJob($category->id)->color}}"><i class="{{findOriginCategoryJob($category->id)->icon}}"></i></div>
                                                                        <div class="category-carousel-item-container">
                                                                            <div class="category-carousel-item-title">{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</div>
                                                                            <div class="category-carousel-item-counter">{{$category->user_services()->count()}} {{__('cms.job')}}</div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <!-- category-carousel-item end -->  
                                                              
                                                            @endforeach
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- category-carousel-wrap end-->  
                                                </div>
                                                <div class="catcar-scrollbar fl-wrap">
                                                    <div class="hs_init"></div>
                                                    <div class="cc-contorl">
                                                        <div class="cc-contrl-item cc-next"><i class="fal fa-angle-left"></i></div>
                                                        <div class="cc-contrl-item cc-prev"><i class="fal fa-angle-right"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--tab end-->