@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">

   
                    <!--  section  end-->
                    <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
                        <div class="container">
                            <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                               
                            </div>
                            <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i class="fal fa-filter"></i>  {{__('cms.filter')}}</div>
                            <div class="fl-wrap">
                                <div class="row">
                                @include('template.products.sections.sidebar-index')

                                    <div class="col-md-8">
                                                              
                                        <!-- listing-item-container -->
                                        <div class="listing-item-container init-grid-items fl-wrap nocolumn-lic">
                                        @foreach ($items as $item)
                                            
                                       
                                            <!-- shop-item  -->
                                            <div class="shop-item">
                                                <div class="shop-item-media">
                                                    <a href="{{route('products.single',['product'=>$item->slug])}}">
                                                       
                                                        @if($item->Hasmedia('images'))
                                                                   <img  src="{{$item->getFirstMediaUrl('images')}}" alt="title" title="title" >

                                                                 
                                                                    @else
                                                                    <img  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title" >

                                                             

                                                               @endif
                                                        <div class="overlay"></div>
                                                    </a>
                                                    <div class="add-tocart color-bg"><span><a  style="color:#fff" href="{{route('products.single',['product'=>$item->slug])}}"> {{__('cms.read_more')}}</a>  </span></div>
                                                   
                                                </div>
                                                <div class="shop-item_title">
                                                    <h4><a href="{{route('products.single',['product'=>$item->slug])}}">{{$item->title}}  </a></h4>
                                                    <span class="shop-item_price">{{$item->price->amount}}€ </span>
                                                    <a href="{{route('products.single',['product'=>$item->slug])}}" class="shop-item_link color-bg">{{__('cms.read_more')}}</a>
                                                </div>
                                            </div>
                                            <!-- shop-item end -->     
                                            @endforeach                                   
                                           
                                        </div>
                                        <!-- listing-item-container end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--section end-->
                    <div class="limit-box fl-wrap"></div>



</div>

@endsection

@section('scripts')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<script src="{{asset('template/js/shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/shop.css?')}}{{uniqid()}}">

@else

<script src="{{asset('template/js/ltr-shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-shop.css?')}}{{uniqid()}}">


@endif

@endsection



