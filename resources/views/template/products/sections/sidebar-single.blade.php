<div class="col-md-4">
                                    <div class=" fl-wrap lws_mobile">
                                        <!--box-widget-item -->
                                        <!-- <div class="box-widget-item fl-wrap block_box">
                                            <div class="box-widget-item-header">
                                                <h3>{{__('cms.search')}} </h3>
                                            </div>
                                            <div class="box-widget fl-wrap">
                                                <div class="box-widget-content">
                                                    <div class="search-widget">
                                                        <form action="#" class="fl-wrap">
                                                            <input name="name" id="se" type="text" class="search" placeholder="{{__('cms.what')}}" value="" />
                                                        </form>
                                                    </div>
                                                </div>
                                                 <div class="box-widget-content">
                                                    <div class="search-widget">
                                                        <form action="#" class="fl-wrap">
                                                            <input name="category" id="se" type="text" class="search" placeholder="{{__('cms.categories')}}" value="" />
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="box-widget-content">
                                                    <div class="search-widget">
                                                        <form action="#" class="fl-wrap">
                                                            <input  type="submit" class="shop-item_link color-bg"   value="{{__('cms.search')}}">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                        </div> -->
                                        <!--box-widget-item end -->  
                                     
                                                          <!--box-widget-item -->
                                                          <!-- <div class="box-widget-item fl-wrap block_box">
                                            <div class="box-widget-item-header">
                                                <h3> {{__('cms.price')}}</h3>
                                            </div>
                                            <div class="box-widget fl-wrap">
                                                <div class="box-widget-content">
                                                    <div class="price-rage-wrap shop-rage-wrap fl-wrap">
                                                        <a href="#" class="srw_btn color-bg" >{{__('cms.update')}}</a>
                                                        <div class="price-rage-item fl-wrap">
                                                            <input type="text" class="shop-price" data-min="{{$min}}" data-max="{{$max}}"  name="shop-price"  data-step="10" value="$$">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <!--box-widget-item end --> 
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap block_box">
                                            <div class="box-widget-item-header">
                                                <h3>{{__('cms.categories')}}  </h3>
                                            </div>
                                            <div class="box-widget fl-wrap">
                                                <div class="box-widget-content">
                                                    <ul class="cat-item no-list-style">
                                                    @foreach($popular_categories as $popular_category)
                                                        <li><a href="#">{!! convert_lang($popular_category,LaravelLocalization::getCurrentLocale(),'title') !!}</a> <span>{{$popular_category->products->count()}}</span></li>
                                                     @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->  
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap block_box">
                                            <div class="box-widget-item-header">
                                                <h3>{{__('cms.products')}}</h3>
                                            </div>
                                            <div class="box-widget fl-wrap">
                                                <div class="box-widget-content">
                                                    <ul class="cart-modal-list no-list-style fl-wrap">
                                                    @foreach ($last_products as $last_product)
                                                        
                                                  
                                                        <li>
                                                            <a class="cart-modal-image" href="product-single.html">
                                                            @if($last_product->Hasmedia('images'))
                                                                 
                                                                 <img src="{{$last_product->getFirstMediaUrl('images')}}" alt="">
                                                               
                                                                  @else
                                                                  <img src="{{asset('template/images/no-image.jpg')}}" alt="">
                                                           

                                                             @endif
                                                           
                                                            </a>
                                                            <div class="cart-modal-det">
                                                                <a href="product-single.html">{{$last_product->title}}</a>
                                                                <div class="quantity"> <span class="woocommerce-Price-amount">{{$last_product->price->amount}}€</span></div>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                      
                                                       
                                                       
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->                                              
                                                                               
                                    </div>
                                </div>