@extends('template.app')

@section('content')

 <!-- content-->
 <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/34.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                              
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section   id="sec1" data-scrollax-parent="true">
                        <div class="container">
                           
                            <div class="pricing-wrap fl-wrap">
                            
                                <!-- price-item-->
                                <div class="price-item best-price">
                                    <div class="price-head   gradient-bg">
                                        <h3> {{__('cms.basic')}}</h3>
                                        <div class="price-num col-dec-2 fl-wrap">
                                            <div class="price-num-item"><span class="mouth-cont">109<span class="curen">€</span></span><span class="year-cont">1010<span class="curen">€</span></span></div>
                                            <div class="clearfix"></div>
                                            <div class="price-num-desc"><span class="mouth-cont">{{__('cms.month')}} </span><span class="year-cont">{{__('cms.year')}} </span></div>
                                        </div>
                                        <div class="circle-wrap" style="right:20%;top:70px;"  >
                                            <div class="circle_bg-bal circle_bg-bal_versmall"></div>
                                        </div>
                                        <div class="circle-wrap" style="right:70%;top:40px;"  >
                                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-150px' }"></div>
                                        </div>
                                        <div class="footer-wave">
                                            <svg viewbox="0 0 100 25">
                                                <path fill="#fff" d="M0 60 V2 Q30 17 55 12 T100 11 V30z" />
                                            </svg>
                                        </div>
                                        <div class="footer-wave footer-wave2">
                                            <svg viewbox="0 0 100 25">
                                                <path fill="#fff" d="M0 90 V16 Q30 7 45 12 T100 5 V30z" />
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="price-content fl-wrap">
                                        <div class="price-desc fl-wrap">
                                            <ul class="no-list-style">
                                                <li>{{__('cms.unlimit')}} </li>
                                               
                                                <li> {{__('cms.support')}} </li>
                                            </ul>
                                            <a href="#" class="price-link rec-link color-bg">{{__('cms.select')}}</a>
                                            <div class="recomm-price">
                                                <i class="fal fa-check"></i> 
                                                {{__('cms.suggestion')}} 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- price-item end-->
                                                                             
                            </div>
                            <span class="section-separator"></span>
                           
                        </div>
                    </section>
                </div>
                <!--content end-->


@endsection
