@extends('template.app')

@section('content')



<!-- Page Breadcrumb -->
<section class="page-breadcrumb">
    <div class="image-layer" style="background-image:url({{asset('template/images/background/1.png')}})"></div>
    <div class="container">
        <div class="clearfix">
            <div class="pull-right">

            </div>
            <div class="pull-left">

            </div>
        </div>
    </div>
</section>
<!-- End Page Breadcrumb -->

<!-- Services Single Section -->
<section class="services-single-section">
    <div class="container">
        <div class="row">
            <!-- Content Column -->
            <div class="content-column col-lg-9 col-md-12 col-sm-12">
                <div class="inner-column">

                    <!-- Services Carousel -->
                    <div class="services-carousel">
                        <div class="image-column">
                            <div class="carousel-outer">

                                <ul class="image-carousel owl-carousel owl-theme">
                                    <li><a href="{{asset('template/images/resource/service-11.jpg')}}" class="lightbox-image"
                                           title="Image Caption Here"><img src="{{asset('template/images/resource/service-11.jpg')}}" alt=""></a></li>
                                    <li><a href="{{asset('template/images/resource/service-2.jpg')}}" class="lightbox-image"
                                           title="Image Caption Here"><img src="{{asset('template/images/resource/service-2.jpg')}}" alt=""></a></li>
                                    <li><a href="{{asset('template/images/resource/service-1.jpg')}}" class="lightbox-image"
                                           title="Image Caption Here"><img src="{{asset('template/images/resource/service-1.jpg')}}" alt=""></a></li>
                                    <li><a href="{{asset('template/images/resource/service-3.jpg')}}" class="lightbox-image"
                                           title="Image Caption Here"><img src="{{asset('template/images/resource/service-3.jpg')}}" alt=""></a></li>

                                </ul>

                                <ul class="thumbs-carousel owl-carousel owl-theme">
                                    <li><img src="{{asset('template/images/resource/service-thumb-1.jpg')}}" alt=""></li>
                                    <li><img src="{{asset('template/images/resource/service-thumb-2.jpg')}}" alt=""></li>
                                    <li><img src="{{asset('template/images/resource/service-thumb-3.jpg')}}" alt=""></li>
                                    <li><img src="{{asset('template/images/resource/service-thumb-4.jpg')}}" alt=""></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="text text-service" style="padding-top: 30px;">
                        {!! convert_lang($item,LaravelLocalization::getCurrentLocale(),'text') !!}
                    </div>


                </div>
            </div>
            <!-- Widgets Column -->
            <div class="widgets-column col-lg-3 col-md-12 col-sm-12">
                <div class="inner-column">

                    <!-- SideBar Widget -->
                    <div class="services-widget category-widget">
                        <ul class="cat-list">
                            @foreach($services as $service)
                            <li class="{{$service->id==$item->id ? 'active' : ''}}"><a href="{{route('services.single',['service'=>convert_lang($service,LaravelLocalization::getCurrentLocale(),'slug')])}}"> {{convert_lang($service,LaravelLocalization::getCurrentLocale(),'title')}}</a></li>

                            @endforeach
                        </ul>
                    </div>

                    <!-- SideBar Widget -->
                    <div class="services-widget schedule-widget">
                        <div class="image">
                            <img src="{{asset('template/images/ad/tabligh.jpg')}}" alt="" />
                            <div class="overlay-box">
                                <div class="content">

                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>



        </div>
    </div>
</section>
<!-- End Services Single Section -->


@endsection

