@extends('template.app')

@section('content')


    <!--page title start-->

    <section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="images/bg/02.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 ml-auto mr-auto">

                </div>
            </div>
            <nav aria-label="breadcrumb" class="page-breadcrumb">

            </nav>
        </div>
    </section>

    <!--page title end-->


    <!--body content start-->

    <div class="page-content">

        <!--accordion start-->

        <section class="faq-content pb-17 sm-pb-8">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="accordion" class="accordion style-1">
                            @foreach($items as $item)
                                @if($loop->first)
                                    <div class="card active">
                                        <div class="card-header">
                                            <h6 class="mb-0">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}</a>
                                            </h6>
                                        </div>
                                        <div id="collapse1" class="collapse show" data-parent="#accordion">
                                            <div class="card-body">
                                                {{convert_lang($item,LaravelLocalization::getCurrentLocale(),'answer')}}
                                            </div>
                                    </div>
                                @else
                                    <div class="card">
                                        <div class="card-header">
                                            <h6 class="mb-0">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">{{$item->title}}</a>
                                            </h6>
                                        </div>
                                        <div id="collapse2" class="collapse" data-parent="#accordion">
                                            <div class="card-body">

                                                {{$item->answer}}
                                            </div>
                                        </div>
                                    </div>

                                    @endif


                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--accordion end-->

    </div>

    <!--body content end-->


@endsection
