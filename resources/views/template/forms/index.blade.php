@extends('template.app')

@section('content')

    <!--page title start-->

    <section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="{{asset('template/images/bg/02.jpg')}}">
        <div class="container">
            <div class="row">
            </div>
            <nav aria-label="breadcrumb" class="page-breadcrumb">

            </nav>
        </div>
    </section>

    <!--page title end-->


    <!--body content start-->

    <div class="page-content">

        <!--about start-->

        <section>
            <div class="container">
                <div class="row align-items-center page-about-us">
                    <div class="col-lg-6 col-md-12">
                        <div class="messages">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @include('core::layout.alert-danger')
                        </div>
                        <form class="demo-form" action="{{route('send.supplier-form')}}" method="POST" style="margin: 100px 10px" id="supplier-form">
                            @csrf
                            <h5>
                                {{__('cms.supplier-form')}}
                            </h5>
                            <div class="form-section">
                                <label for="kind">{{__('cms.kind')}} *:</label>
                                <p>

                                    {{__('cms.manufacturer')}}: <input type="radio" name="kind"  value="manufacturer" required="">

                                    {{__('cms.vendor-company')}}: <input type="radio" name="kind"  value="vendor-company" >

                                    {{__('cms.shop')}}: <input type="radio" name="kind"  value="shop" >
                                </p>

                                <label for="company-name">{{__('cms.company-name')}} *:</label>
                                <input type="text" class="form-control" name="company_name" required="" autocomplete="off">



                                <label for="company-type">{{__('cms.company-type')}} *:</label>
                                <p>


                                    {{__('cms.private-equity')}}: <input type="radio" name="company_type"  value="private-equity" required="">


                                    {{__('cms.with-limited-responsibility')}}: <input type="radio" name="company_type"  value="with-limited-responsibility" >


                                    {{__('cms.other-cases')}}: <input type="radio" name="company_type"  value="other-cases" >
                                </p>

                                <label for="field-of-activity">{{__('cms.field-of-activity')}} *:</label>
                                <p>


                                    {{__('cms.production')}}: <input type="radio" name="field_of_activity"  value="production" required="">


                                    {{__('cms.sales')}}: <input type="radio" name="field_of_activity"  value="sales" >


                                    {{__('cms.services')}}: <input type="radio" name="field_of_activity"  value="services" >
                                </p>
                                <label for="type-of-ownership">{{__('cms.type-of-ownership')}} *:</label>
                                <p>


                                    {{__('cms.private')}}: <input type="radio" name="type_of_ownership"  value="private" required="">


                                    {{__('cms.governmental')}}: <input type="radio" name="type_of_ownership"  value="governmental" >


                                    {{__('cms.other-cases')}}: <input type="radio" name="type_of_ownership"  value="other-cases" >
                                </p>

                                <label for="product-name">{{__('cms.product-name')}}:</label>
                                <input type="text" class="form-control" name="product_name" required="" autocomplete="off">

                                <label for="registration-number">{{__('cms.registration-number')}}:</label>
                                <input type="text" class="form-control" name="registration_number" required="" autocomplete="off">

                                <label for="operating-license-number">{{__('cms.operating-license-number')}}:</label>
                                <input type="text" class="form-control" name="operating_license_number" required="" autocomplete="off">

                                <label for="company-name">{{__('cms.economic-code')}}:</label>
                                <input type="text" class="form-control" name="economic_code" required="" autocomplete="off">

                                <label for="national-id">{{__('cms.national-id')}}:</label>
                                <input type="text" class="form-control" name="national_id" required="" autocomplete="off">

                                <label for="business-license-number">{{__('cms.business-license-number')}}:</label>
                                <input type="text" class="form-control" name="business_license_number" required="" autocomplete="off">


                                <label for="date-of-establishment">{{__('cms.date-of-establishment')}}:</label>
                                <input type="text" class="form-control" name="date_of_establishment" required="" autocomplete="off">

                            </div>
                            <div class="form-section">

                                <label for="licensee-name">{{__('cms.licensee-name')}} *:</label>
                                <input type="text" class="form-control" name="licensee_name" required="" autocomplete="off">


                                <label for="national-code-of-the-licensee">{{__('cms.national-code-of-the-licensee')}} *:</label>
                                <input type="text" class="form-control" name="national_code_of_the_licensee" required="" autocomplete="off">

                                <label for="the-address-of-central-office">{{__('cms.the-address-of-central-office')}} *:</label>
                                <input type="text" class="form-control" name="the_address_of_central_office" required="" autocomplete="off">


                                <label for="telephone-central-office">{{__('cms.telephone-central-office')}} *:</label>
                                <input type="text" class="form-control" name="telephone_central_office" required="" autocomplete="off">

                                <label for="head-office-fax">{{__('cms.head-office-fax')}} *:</label>
                                <input type="text" class="form-control" name="head_office_fax" required="" autocomplete="off">

                                <label for="factory-address">{{__('cms.factory-address')}} :</label>
                                <input type="text" class="form-control" name="factory_address" autocomplete="off">

                                <label for="factory-phone">{{__('cms.factory-phone')}} :</label>
                                <input type="text" class="form-control" name="factory_phone" autocomplete="off">

                                <label for="factory-fax">{{__('cms.factory-fax')}} :</label>
                                <input type="text" class="form-control" name="factory_fax" autocomplete="off">

                                <label for="company-email">{{__('cms.company-email')}} :</label>
                                <input type="text" class="form-control" name="company_email" autocomplete="off">

                                <label for="company-website">{{__('cms.company-website')}} :</label>
                                <input type="text" class="form-control" name="company_website" autocomplete="off">

                                <label for="company-capital">{{__('cms.company-capital')}} *:</label>
                                <input type="text" class="form-control" name="company_capital" required="" autocomplete="off">


                                <label for="names-of-board-members">{{__('cms.names-of-board-members')}} *:</label>
                                <input type="text" class="form-control" name="names_of_board_members" required="" autocomplete="off">

                                <label for="are-you-a-representative-if-yes-what-kind-of-representation">{{__('cms.are-you-a-representative-if-yes-what-kind-of-representation')}} *:</label>
                                <input type="text" class="form-control" name="are_you_a_representative_if_yes_what_kind_of_representation" required="" autocomplete="off">


                                <label for="is-there-a-variety-of-brands-in-the-goods-you-supply">{{__('cms.is-there-a-variety-of-brands-in-the-goods-you-supply')}} *:</label>
                                <input type="text" class="form-control" name="is_there_a_variety_of_brands_in_the_goods_you_supply" required="" autocomplete="off">


                                <label for="do-you-use-the-services-of-insurance-companies-for-employees-products-devices-and-buildings-explain">{{__('cms.do-you-use-the-services-of-insurance-companies-for-employees-products-devices-and-buildings-explain')}} *:</label>
                                <input type="text" class="form-control" name="do_you_use_the_services_of_insurance_companies_for_employees_products_devices_and_buildings_explain" required="" autocomplete="off">



                            </div>
                            <div class="form-section">

                                <label for="do-you-have-a-stock-for-the-products-or-products-you-supply">{{__('cms.do-you-have-a-stock-for-the-products-or-products-you-supply')}} *:</label>
                                <input type="text" class="form-control" name="do_you_have_a_stock_for_the_products_or_products_you_supply" required="" autocomplete="off">


                                <label for="warehouse-capacity">{{__('cms.warehouse-capacity')}} *:</label>
                                <input type="text" class="form-control" name="warehouse_capacity" required="" autocomplete="off">

                                <label for="number-of-warehouses">{{__('cms.number-of-warehouses')}} *:</label>
                                <input type="text" class="form-control" name="number_of_warehouses" required="" autocomplete="off">


                                <label for="annual-production-capacity">{{__('cms.annual-production-capacity')}} :</label>
                                <input type="text" class="form-control" name="annual_production_capacity" required="" autocomplete="off">

                                <label for="production-volume-of-the-last-three-years">{{__('cms.production-volume-of-the-last-three-years')}} :</label>
                                <input type="text" class="form-control" name="production_volume_of_the_last_three_years" required="" autocomplete="off">

                                <label for="the-average-annual-turnover-of-the-current-year">{{__('cms.the-average-annual-turnover-of-the-current-year')}} *:</label>
                                <input type="text" class="form-control" name="the_average_annual_turnover_of_the_current_year" autocomplete="off">

                                <label for="money-in-circulation-Annual-average-of-the-last-5-years">{{__('cms.money-in-circulation-Annual-average-of-the-last-5-years')}} *:</label>
                                <input type="text" class="form-control" name="money_in_circulation_Annual_average_of_the_last_5_years" autocomplete="off">


                                <label for="if-you-are-a-manufacturer-select-the-status-of-the-factory-or-workshop">{{__('cms.if-you-are-a-manufacturer-select-the-status-of-the-factory-or-workshop')}} *:</label>
                                <p>


                                    {{__('cms.the-factory-or-workshop-you-own')}}: <input type="radio" name="if_you_are_a_manufacturer_select_the_status_of_the_factory_or_workshop"  value="the-factory-or-workshop-you-own" required="">


                                    {{__('cms.factory-or-workshop-under-your-official-rent')}}: <input type="radio" name="if_you_are_a_manufacturer_select_the_status_of_the_factory_or_workshop"  value="factory-or-workshop-under-your-official-rent" >

                                </p>



                                <label for="does-the-product-offered-by-you-have-a-warranty-guarantee-and-after-sales-service">{{__('cms.does-the-product-offered-by-you-have-a-warranty-guarantee-and-after-sales-service')}} :</label>
                                <p>


                                    {{__('cms.yes')}}: <input type="radio" name="does_the_product_offered_by_you_have_a_warranty_guarantee_and_after_sales_service"  value="yes" required="">


                                    {{__('cms.no')}}: <input type="radio" name="does_the_product_offered_by_you_have_a_warranty_guarantee_and_after_sales_service"  value="no" >

                                </p>


                                <label for="do-you-offer-technical-advice-in-your-field-of-work">{{__('cms.do-you-offer-technical-advice-in-your-field-of-work')}} :</label>
                                <p>


                                    {{__('cms.yes')}}: <input type="radio" name="do_you_offer_technical_advice_in_your_field_of_work"  value="yes" required="">


                                    {{__('cms.no')}}: <input type="radio" name="do_you_offer_technical_advice_in_your_field_of_work"  value="no" >

                                </p>


                                <label for="is-your-technical-advice-free-if-you-do-not-buy">{{__('cms.is-your-technical-advice-free-if-you-do-not-buy')}} :</label>
                                <p>


                                    {{__('cms.yes')}}: <input type="radio" name="is_your_technical_advice_free_if_you_do_not_buy"  value="yes" required="">


                                    {{__('cms.no')}}: <input type="radio" name="is_your_technical_advice_free_if_you_do_not_buy"  value="no" >

                                </p>


                                <label for="is-your-technical-advice-free-to-buy">{{__('cms.is-your-technical-advice-free-to-buy')}} :</label>
                                <p>


                                    {{__('cms.yes')}}: <input type="radio" name="is_your_technical_advice_free_to_buy"  value="yes" required="">


                                    {{__('cms.no')}}: <input type="radio" name="is_your_technical_advice_free_to_buy"  value="no" >

                                </p>





                                <label for="number-of-successful-contracts-in-the-past-year-for-the-goods-under-evaluation-with-the-cement-steel-oil-and-petrochemical-industries-mines">{{__('cms.number-of-successful-contracts-in-the-past-year-for-the-goods-under-evaluation-with-the-cement-steel-oil-and-petrochemical-industries-mines')}} *:</label>
                                <input type="text" class="form-control" name="number_of_successful_contracts_in_the_past_year_for_the_goods_under_evaluation_with_the_cement_steel_oil_and_petrochemical_industries_mines" required="" autocomplete="off">


                                <label for="number-of-successful-contracts-in-the-past-year-with-companies-under-espandar-for-the-product-under-evaluation">{{__('cms.number-of-successful-contracts-in-the-past-year-with-companies-under-espandar-for-the-product-under-evaluation')}} *:</label>
                                <input type="text" class="form-control" name="number_of_successful_contracts_in_the_past_year_with_companies_under_espandar_for_the_product_under_evaluation" required="" autocomplete="off">



                            </div>

                            <div class="form-section">

                                <label for="does-the-product-you-produce-or-supply-have-iran-code">{{__('cms.does-the-product-you-produce-or-supply-have-iran-code')}} *:</label>
                                <input type="text" class="form-control" name="does_the_product_you_produce_or_supply_have_iran_code" required="" autocomplete="off">


                                <label for="do-you-use-the-services-of-other-companies-or-factories">{{__('cms.do-you-use-the-services-of-other-companies-or-factories')}} *:</label>
                                <input type="text" class="form-control" name="do_you_use_the_services_of_other_companies_or_factories" required="" autocomplete="off">

                                <label for="explain-if-you-use-the-services-of-other-companies">{{__('cms.explain-if-you-use-the-services-of-other-companies')}} *:</label>
                                <input type="text" class="form-control" name="explain_if_you_use_the_services_of_other_companies" required="" autocomplete="off">


                                <label for="does-your-product-have-standard-name-it">{{__('cms.does-your-product-have-standard-name-it')}} *:</label>
                                <input type="text" class="form-control" name="does_your_product_have_standard_name_it" required="" autocomplete="off">

                                <label for="is-your-production-method-below-certain-standard-name-it">{{__('cms.is-your-production-method-below-certain-standard-name-it')}} :</label>
                                <input type="text" class="form-control" name="is_your_production_method_below_certain_standard_name_it" required="" autocomplete="off">

                                <label for="have-you-obtained-certificates-of-quality-management-environment-etc-explain">{{__('cms.have-you-obtained-certificates-of-quality-management-environment-etc-explain')}} *:</label>
                                <input type="text" class="form-control" name="have_you_obtained_certificates_of_quality_management_environment_etc_explain" >

                                <label for="do-you-have-unit-called-research-and-development">{{__('cms.do-you-have-unit-called-research-and-development')}} *:</label>
                                <input type="text" class="form-control" name="do_you_have_unit_called_research_and_development" autocomplete="off">



                                <label for="is-the-quality-control-work-of-the-product-or-production-service-done-in-your-collection">{{__('cms.is-the-quality-control-work-of-the-product-or-production-service-done-in-your-collection')}} *:</label>
                                <input type="text" class="form-control" name="is_the_quality_control_work_of_the_product_or_production_service_done_in_your_collection" required="" autocomplete="off">


                                <label for="number-of-staff-below-the-diploma-set-but-with-specialization">{{__('cms.number-of-staff-below-the-diploma-set-but-with-specialization')}} *:</label>
                                <input type="text" class="form-control" name="number_of_staff_below_the_diploma_set_but_with_specialization" required="" autocomplete="off">



                            </div>


                            <div class="form-section">

                                <label for="is-there-codified-training-system-for-the-unit-staff">{{__('cms.is-there-codified-training-system-for-the-unit-staff')}} *:</label>
                                <input type="text" class="form-control" name="is_there_codified_training_system_for_the_unit_staff" required="" autocomplete="off">


                                <label for="number-of-third-party-inspector-certification-during-the-past-year">{{__('cms.number-of-third-party-inspector-certification-during-the-past-year')}} *:</label>
                                <input type="text" class="form-control" name="number_of_third_party_inspector_certification_during_the_past_year" required="" autocomplete="off">

                                <label for="do-you-have-history-of-exporting-product-or-service-during-the-last-5-years">{{__('cms.do-you-have-history-of-exporting-product-or-service-during-the-last-5-years')}} *:</label>
                                <input type="text" class="form-control" name="do_you_have_history_of_exporting_product_or_service_during_the_last_5_years" required="" autocomplete="off">


                                <label for="export-amount-during-the-last-5-years">{{__('cms.export-amount-during-the-last-5-years')}} *:</label>
                                <input type="text" class="form-control" name="export_amount_during_the_last_5_years" required="" autocomplete="off">

                                <label for="do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company">{{__('cms.do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company')}} :</label>
                                <input type="text" class="form-control" name="do_you_have_the_facilities_to_transport_the_goods_to_the_place_of_the_buyer_company" required="" autocomplete="off">

                                <label for="do-you-have-distribution-network-if-yes-please-name-the-distribution-locations">{{__('cms.do-you-have-distribution-network-if-yes-please-name-the-distribution-locations')}} *:</label>
                                <input type="text" class="form-control" name="do_you_have_distribution_network_if_yes_please_name_the_distribution_locations" autocomplete="off">

                                <label for="do-you-have-equipment-for-packing-goods">{{__('cms.do-you-have-equipment-for-packing-goods')}} *:</label>
                                <input type="text" class="form-control" name="do_you_have_equipment_for_packing_goods" autocomplete="off">



                                <label for="are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it">{{__('cms.are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it')}} *:</label>
                                <input type="text" class="form-control" name="are_you_member_of_research_institute_research_associations_trade_unions_unions_etc_name_it" required="" autocomplete="off">


                                <label for="how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much">{{__('cms.how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much')}} *:</label>
                                <input type="text" class="form-control" name="how_many_days_after_the_purchased_or_repaired_goods_stay_in_your_warehouse_do_you_receive_storage_costs_and_how_much" required="" autocomplete="off">



                            </div>

                            <div class="form-navigation">
                                <button type="button" class="previous btn btn-info pull-left">{{__('cms.previous')}}</button>
                                <button type="button" class="next btn btn-info pull-right">{{__('cms.next')}}</button>
                                <input type="submit" class="btn btn-info pull-right" value="{{__('cms.send')}}">
                                <span class="clearfix"></span>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!--about end-->



    </div>

    <!--body content end-->
@endsection

@section('heads')

{{--<link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">--}}
<link href="{{asset('template/css/parsley.css')}}" rel="stylesheet" type="text/css">

    <style>
        .form-section {
            padding-left: 15px;
            border-left: 2px solid #FF851B;
            display: none;
        }
        .form-section.current {
            display: inherit;
        }
        .btn-info, .btn-default {
            margin-top: 10px;
        }
        .btn-info{color:#fff;background-color:#5bc0de;border-color:#5bc0de}
        .btn-info:hover{color:#fff;background-color:#31b0d5;border-color:#2aabd2}
        .btn-info.focus,.btn-info:focus{color:#fff;background-color:#31b0d5;border-color:#2aabd2}
    </style>
@endsection

@section('scripts')

    <script src="{{asset('template/js/parsley.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            var $sections = $('.form-section');

            function navigateTo(index) {
                // Mark the current section with the class 'current'
                $sections
                    .removeClass('current')
                    .eq(index)
                    .addClass('current');
                // Show only the navigation buttons that make sense for the current section:
                $('.form-navigation .previous').toggle(index > 0);
                var atTheEnd = index >= $sections.length - 1;
                $('.form-navigation .next').toggle(!atTheEnd);
                $('.form-navigation [type=submit]').toggle(atTheEnd);
            }

            function curIndex() {
                // Return the current index by looking at which section has the class 'current'
                return $sections.index($sections.filter('.current'));
            }

            // Previous button is easy, just go back
            $('.form-navigation .previous').click(function() {
                navigateTo(curIndex() - 1);
            });

            // Next button goes forward iff current block validates
            $('.form-navigation .next').click(function() {
                $('.demo-form').parsley().whenValidate({
                    group: 'block-' + curIndex()
                }).done(function() {
                    navigateTo(curIndex() + 1);
                });
            });

            // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
            $sections.each(function(index, section) {
                $(section).find(':input').attr('data-parsley-group', 'block-' + index);
            });
            navigateTo(0); // Start at the beginning
        });
    </script>

@endsection








