<?php

namespace App\View;



use Illuminate\View\View;

use Modules\Core\Entities\ListServices;



class searchWerbungComposer{


    public function compose(View $view){

        $view->with('all_parent_category_bussiness', ListServices::orderBy('order','asc')->where('parent',0)->get());
        $view->with('more_view_category_bussiness', ListServices::with('user_services')
        ->whereHas('user_services',function($query){
                $query->whereStatus(2);
        })->get()->sortByDESC(function($query){
            return $query->user_services->count();
        })->take(5));
     
    }

}
