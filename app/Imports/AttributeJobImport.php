<?php

namespace App\Imports;

use Modules\Advertising\Entities\AttributeJob;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AttributeJobImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new AttributeJob([

            'title'=>$row['title'],
            'token'=>tokenGenerate()

        ]);
    }
}
