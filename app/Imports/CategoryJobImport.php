<?php

namespace App\Imports;

use Modules\Core\Entities\ListServices;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CategoryJobImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
       
     
       
        return new ListServices([
          
            'title'=>$row['title'],
            'parent'=>$row['parent'],
            'order'=>intval($row['order']),
            'pattern'=>$row['pattern'],
            'status'=>1,
            'token'=>tokenGenerate()

        ]);
    }
}
