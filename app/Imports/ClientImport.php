<?php

namespace App\Imports;

use Illuminate\Support\Facades\Hash;
use Modules\Client\Entities\Client;
use Maatwebsite\Excel\Concerns\ToModel;

class ClientImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Client([
            'first_name'=>$row['first_name'],
            'last_name'=>$row['last_name'],
            'email'=>$row['email'],
            'password'=>Hash::make($row['password']),
            'token'=>tokenGenerate()
        ]);
    }
}
