<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Modules\Menu\Entities\Menu;

class MenuImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Menu([
           
            'user'=>intval($row['user']),
            'title'=>$row['title'],
            'symbol'=>$row['symbol'],
            'pattern'=>$row['pattern'],
            'List_menus'=>$row['List_menus'],
            'href'=>$row['href'],
            'order'=>$row['order'],
            'parent'=>$row['parent'],
            'token'=>tokenGenerate()

        ]);
    }
}
