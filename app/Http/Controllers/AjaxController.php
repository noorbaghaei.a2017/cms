<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Ad\Entities\Ad;
use Modules\Core\Entities\Category;
use Modules\Chat\Entities\Chat;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Core\Entities\Area;
use Modules\Core\Entities\UserServices;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Favorite;
use Modules\Core\Entities\Rate;
use Modules\Core\Entities\AnalyticsClient;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\State;
use Modules\Core\Entities\ListServices;
use Modules\Core\Entities\User;
use Modules\Product\Entities\CartList;
use Modules\Product\Entities\Product;
use Illuminate\Support\Facades\Auth;

class AjaxController extends Controller
{
    public function verifyMobileSms($user,$code){
        $user=Client::whereToken($user)->first();
        if($user->code==$code){
            $user->update([
                'is_active'=>2,
                'expire'=>now()
            ]);

            auth('client')->loginUsingId($user->id);

            $msg="success";
            $data=[
                'result'=>1
            ];
            $status=200;
        }
        else{
            $msg="کد نامعتبر است";
            $data=[
                'result'=>null
            ];
            $status=400;
        }
        return response()->json(array('msg'=>$msg,'data'=>$data),$status);
    }

    public function verifyEmailCode($email,$code){

        if(Client::whereEmail($email)->first()){
        $client=Client::whereEmail($email)->first();
        if($client->code==$code){
            $client->update([
                'is_active'=>2,
                'expire'=>now()
            ]);

            auth('client')->loginUsingId($client->id);

            
            $data=[
                'result'=>true,
                'secret'=>$client->client_token
            ];
            $status=200;
        }
        else{
           
            $data=[
                'result'=>false
            ];
            $status=400;
        }
        return response()->json(array('msg'=>'','data'=>$data),200);
    }
    else{
      
        $data=[
            'result'=>false
        ];
        $status=400;
    }
        return response()->json(array('msg'=>'','data'=>$data),200);
    }


    public function Login(Request $request){

       
        
        if(Client::whereEmail($request->email)->first()){
            $client=Client::whereEmail($request->email)->first();
            if(Hash::check($request->password,$client->password)){

                if($client->is_verify_email==1){
                    auth('client')->loginUsingId($client->id);
                    $data=[
                        'status'=>true,
                       'code'=>200
                    ];
                }else{
                    
                    $data=[
                        'status'=>false,
                        'code'=>500
                        
                    ];
                }
               
            }
            else{

                $data=[
                    'status'=>false,
                   'code'=>500
                ];
            }
           
        }
       

        else{

            $data=[
                'status'=>false,
                'code'=>500
            ];
        }
        return response()->json(array('result'=>$data),$data['code']);

    }

    public function Register(Request $request){


        if($request->law=="on" && $request->impressum=="on"){

            $item=[
                'email'=>($request->has('email')) ? $request->email : null,
                'password'=>($request->has('password')) ? $request->password : null,
                'first'=>($request->has('first')) ? $request->first : null,
                'last'=>($request->has('last')) ? $request->last : null,
                'law'=>1,
                'impressum'=>1
            
            ];
            
            
            }
            else{
            $item=[
                'email'=>($request->has('email')) ? $request->email : null,
                'password'=>($request->has('password')) ? $request->password : null,
                'first'=>($request->has('first')) ? $request->first : null,
                'last'=>($request->has('last'))  ? $request->last : null,
               
            
            ];
            
            
            }
            
            
            
            $validator = Validator::make($item, [
            'email'=>'unique:clients|required|email',
            'password'=>'required|min:8',
            'law'=>'required',
            'impressum'=>'required',
            'first'=>'required',
            'last'=>'required'
            ]);
            
            if($validator->fails()){
            
            $errors=$validator->errors();
            
            $data=[
                        'status'=>true,
                        'errors'=>$errors,
                        'validate_error'=>true
            ];
            return response()->json(array('msg'=>'no','result'=>$data),200);
            
            }
            else{

                $code=5044;
                $client=Client::create([
                    'email'=>$request->email,
                    'first_name'=>$request->first,
                    'last_name'=>$request->last,
                    'password'=>Hash::make($request->password),
                    'code'=>$code,
                    'law'=>1,
                    'impressum'=>1,
                    'notification_email'=>($request->notification_email=="on") ? 1 : 0,
                    'code_expire'=>now()->addMinutes(10),
                    'role'=>ClientRole::whereTitle('user')->firstOrFail()->id,
                    'ip'=>$_SERVER['REMOTE_ADDR'],
                    'device'=>$_SERVER['HTTP_USER_AGENT'],
                    'verify_time'=>now(),
                    'token'=>tokenGenerate(),
                    'client_token'=>tokenResetGenerate(),
                ]);
            
                $client->myCode()->create([
                    'code'=>generateCode()
                ]);
            
                $client->update([
                    'is_active'=>2,
                    'expire'=>now()
            
                ]);
            
                $client->info()->create();
            
                $client->analyzer()->create();
            
                $client->company()->create();
            
                $client->seo()->create();
            
            
                $client->wallet()->create([
                    'score'=>500
                ]);
            
                $client->cart()->create([
                    'client'=>$client->id,
                    'mobile'=>""
                ]);
            
            
                if($client){
                   
                    $data=[
                        'status'=>true,
                        'validate_error'=>false,
                        'secret'=>$client->client_token,
                    ];
                   
                    $result=[
                        'token'=>$client->token,
                        'secret'=>$client->client_token,
                        'email'=>$client->email,
                        'lastname'=>$client->last_name,
                        'firstname'=>$client->first_name,
                        'title'=>'Bestätigung',

                    ];
                   
                    sendNotificationCustomEmail($result,'emails.front.verify-email');

                    return response()->json(array('msg'=>'ok','result'=>$data),200);
                   
                }
                else{

                    $data=[
                        'status'=>false,
                        'validate_error'=>false
                    ];
                    return response()->json(array('msg'=>'no','result'=>$data),500);
                }

               
            }

       
    
       
       

    }

    public function verifyToken($token){

        if(Client::whereToken($token)->first()){

            $client=Client::whereToken($token)->first();
            auth('client')->loginUsingId($client->id);
            $data=[
                'result'=>true,
            ];
        }

        else{

            $data=[
                'result'=>false,
            ];
        }
        return response()->json(array('msg'=>'ok','data'=>$data),200);
    }


    public function storeFavoriteJob($id){

        $job=UserServices::findOrFail($id);

       

        if(Favorite::where('favoriteable_type','Modules\Core\Entities\UserServices')->where('favoriteable_id',$job->id)->where('client',auth('client')->user()->id)->first()){
           
            $favorite=Favorite::where('favoriteable_type','Modules\Core\Entities\UserServices')->where('favoriteable_id',$job->id)->where('client',auth('client')->user()->id)->first();
           
            $favorite->delete();
            $data=[
                'result'=>false,
            ];
        }
        else{

            $job->favorites()->create([
                'client'=>auth('client')->user()->id,
            ]);
            $data=[
                'result'=>true,
            ];
            
           
        }

       
        return response()->json(array('msg'=>'ok','data'=>$data),200);

        
        
       
    }


    public function setCoockieClient($ip,$analyitic){
        


       if($ip==1 && $analyitic==1){

            if(!AnalyticsClient::whereIp($_SERVER['REMOTE_ADDR'])->first()){
                $items=AnalyticsClient::create([
                    'ip'=>$_SERVER['REMOTE_ADDR'],
                    'analytic'=> 1,
                    'device'=>$_SERVER['HTTP_USER_AGENT'],
                ]);
    
               $data=[
                   'result'=>true,
                   'item'=>$items
               ];
            }
        }
           
        
        return response()->json(array('msg'=>'ok','data'=>$data),200);
       
        
    }


    
    public function verifyEmail(Request $request){
        $code=numberCode();
       
        if((Client::whereEmail($request->email)->first())){
            $client=Client::whereEmail($request->email)->update([
                'code'=>$code,
                'code_expire'=>now()->addMinutes(10),
            ]);
            $items=[
                'result'=>true,
                'email'=>$request->email,
                'code'=>$code
            ];
            $status=200;
            $data=[
                'code'=>$code,
                'email'=>$request->email,
                'title'=>'Iniaz Verify Code',
                'text'=>'Bestätigung',
               
             ];
         
           sendNotificationCustomEmail($data,'emails.front.otp-email');

        }
        else{
            $items=[
                'result'=>false,
                'email'=>$request->email,
                'code'=>$code
            ];
            $status=200;
        }

    
        return response()->json(array('msg'=>'ok','data'=>$items),200);
    }


    public function loadCountries(){

        $item=Country::latest()->get()->toArray();

            $msg="success";
            $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function loadStates($country){

        $item=State::latest()->where('country',$country)->get()->toArray();
        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function loadCities($state){

        $item=City::latest()->where('state',$state)->get()->toArray();

        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function loadAreas($city)
    {

        $item = Area::latest()->where('city', $city)->get()->toArray();

        $msg = "success";
        $status = 200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);

    }
        public function countAd($ad){

        $item=Ad::where('token',$ad)->first();
        $item->analyzer->increment('view');

        $item=[
            'item'=>$item,
            'token'=>$ad
        ];

        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function incProduct($token){

       $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
       $data=CartList::whereCart($client->cart->id)->whereProduct($product->id)->first();

        $update=$data->update([
           'count'=>$data->count + 1,
            'price'=>$data->price + intval($product->price->amount)
       ]);
        $totalCart=CartList::whereCart($client->cart->id)->get();

        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($update){
            $item=[
                'method'=>'increment',
                'current'=>$data,
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'increment',
                'count'=>null,
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }

    public function decProduct($token){

        $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
        $data=CartList::whereCart($client->cart->id)->whereProduct($product->id)->first();

        $update=$data->update([
            'count'=>$data->count - 1,
            'price'=>$data->price - intval($product->price->amount)
        ]);
        $totalCart=CartList::whereCart($client->cart->id)->get();
        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($update){
            $item=[
                'method'=>'decrement',
                'current'=>$data,
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'decrement',
                'current'=>null,
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function removeProduct($token){

        $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
        $deleted=CartList::whereCart($client->cart->id)->whereProduct($product->id)->delete();
        $totalCart=CartList::whereCart($client->cart->id)->get();
        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($deleted){
            $item=[
                'method'=>'remove',
                'result'=>$totalCart->count(),
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'remove',
                'result'=>$totalCart->count(),
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }

    public function showMessage(Request $request){

        $items=Chat::where('client',auth('client')->user()->id)->get();


        foreach ($items as $item) {

            if($item->user!=null){
                $item->update([
                    'status'=>2
                ]);
            }


        }

        return response()->json(['result'=>$items],200);

    }
    public function loadCityWithPostalCode($code){

        if(!\apiPostGerman($code)){

            $data=[
                'result'=>"",
                'status'=>false
            ];
        }
        else{
          
            $data=[
                'result'=>\apiPostGerman($code),
                'status'=>true
               
            ];
        }
       

        return response()->json(array('msg'=>"ok",'data'=>$data),200);

    }
    public function loadSubCategoryWithBussiness($category,$lang){

        $array=ListServices::with('translates')->whereParent($category)->get();
        $items=[];

        foreach($array as $item){
           $items[]=[
            'id'=>$item->id,
            'translate'=>$item->translates()->where('lang',$lang)->count() > 0 ? $item->translates()->where('lang',$lang)->first()->title : $item->title,
          
            
           ];
        }
     
            $data=[
                'result'=>$items,
                'status'=>true
               
            ];

        return response()->json(array('msg'=>"ok",'data'=>$data),200);

    }
    public function loadSubCategoryWithProduct($category,$lang){

        $array=Category::with('translates')->where('model','Modules\Product\Entities\Product')->whereParent($category)->get();
        $items=[];

        foreach($array as $item){
           $items[]=[
            'id'=>$item->id,
            'translate'=>$item->translates()->where('lang',$lang)->count() > 0 ? $item->translates()->where('lang',$lang)->first()->title : $item->title,
          
            
           ];
        }
     
            $data=[
                'result'=>$items,
                'status'=>true
               
            ];

        return response()->json(array('msg'=>"ok",'data'=>$data),200);

    }

    public function loadSubCategoryWithBussinessWithoutLang($category){

        $array=ListServices::whereParent($category)->get();
        $items=[];

        foreach($array as $item){
           $items[]=[
            'id'=>$item->id,
            'title'=>$item->title,
          
           ];
        }
     
            $data=[
                'result'=>$items,
                'status'=>true
               
            ];

        return response()->json(array('msg'=>"ok",'data'=>$data),200);

    }

    
    public function loadInfoWithAPiGermanList($info){



        $data=UserServices::distinct()->where('city','like',$info.'%')->where('status',2)->pluck('city')->take(3)->toArray();

        return response()->json(array('msg'=>"ok",'data'=>$data),200);

    }
    public function loadInfoProductWithAPiGermanList($info){



        $data=Product::distinct()->where('city','like',$info.'%')->where('status',2)->pluck('city')->take(3)->toArray();

        return response()->json(array('msg'=>"ok",'data'=>$data),200);

    }
    public function searchCategoryJob($title,$lang){
        
        $items=[];

      

        if($lang=='fa'){
            $lists=ListServices::select('id','title')->with('translates')
            ->whereHas('translates',function($query) use($title){
                $query->where('title','like',trim($title).'%')->whereLang('fa');
        })->take(3)->get();
  
        }elseif($lang=='en'){
            $lists=ListServices::select('id','title')->with('translates')
            ->whereHas('translates',function($query) use($title){
                $query->where('title','like',trim($title).'%')->whereLang('en');
        })->take(3)->get();
        }
        elseif($lang=='de'){
            $lists=ListServices::select('id','title')->with('translates')->where('title','like',trim($title).'%')->take(3)->get();
  
        }

        foreach($lists as $item){
            $items[]=[
             'id'=>$item->id,
             'title'=>str_replace ( ' ', '+', $item->title),
             'translate'=>$item->translates()->where('lang',$lang)->count() > 0 ? $item->translates()->where('lang',$lang)->first()->title : $item->title,
     
            ];
         }
            $data=[
                'lists'=>$lists,
                'title'=>str_replace ( ' ', '+', $title),
                'result'=>$items,
                'status'=>true
            ];
      
  
        return response()->json(array('msg'=>"ok",'data'=>$data),200);

    }

    public function searchCategoryProduct($title,$lang){
        
        $items=[];

      

        if($lang=='fa'){
            $lists=Category::with('translates')->where('model','Modules\Product\Entities\Product')
            ->whereHas('translates',function($query) use($title){
                $query->where('title','like',trim($title).'%')->whereLang('fa');
        })->take(3)->get();
  
        }elseif($lang=='en'){
            $lists=Category::with('translates')->where('model','Modules\Product\Entities\Product')
            ->whereHas('translates',function($query) use($title){
                $query->where('title','like',trim($title).'%')->whereLang('en');
        })->take(3)->get();
        }
        elseif($lang=='de'){
            $lists=Category::with('translates')->where('model','Modules\Product\Entities\Product')->where('title','like',trim($title).'%')->take(3)->get();
  
        }

        foreach($lists as $item){
            $items[]=[
             'id'=>$item->id,
             'title'=>str_replace ( ' ', '+', $item->title),
             'translate'=>$item->translates()->where('lang',$lang)->count() > 0 ? $item->translates()->where('lang',$lang)->first()->title : $item->title,
     
            ];
         }
            $data=[
                'lists'=>$lists,
                'title'=>str_replace ( ' ', '+', $title),
                'result'=>$items,
                'status'=>true
            ];
      
  
        return response()->json(array('msg'=>"ok",'data'=>$data),200);

    }

    public function setRateJob($token,$rate){

        
        if($rate=="1" || $rate=="2" || $rate=="3" || $rate=="4" || $rate=="5"){
           
        }else{
            $rate='5';
        }
      
        if(!UserServices::whereToken($token)->first()){
            $data=[
                'status'=>false,
              
            ];
            return response()->json(array('msg'=>"ok",'data'=>$data),200);
        }




        $job=UserServices::whereToken($token)->first();

        if(UserServices::where('user',auth('client')->user()->id)->where('id',$job->id)->first()){
            $data=[
                'status'=>false,
              
            ];
            return response()->json(array('msg'=>"ok",'data'=>$data),200);
        }
       

        if(Rate::where('rateable_type','Modules\Core\Entities\UserServices')->where('rateable_id',$job->id)->where('client',auth('client')->user()->id)->first()){
           
       $rate_item=Rate::where('rateable_type','Modules\Core\Entities\UserServices')->where('rateable_id',$job->id)->where('client',auth('client')->user()->id)->first();
           
      
       $rate_item->update([
           'rate'=>$rate
       ]);
       $rate_count=$job->rates->sum('rate')==0 ? 0 : $job->rates->sum('rate') / $job->rates->count();

       $data=[
                'status'=>true,
                'rate'=>$rate_count
               
            ];

        return response()->json(array('msg'=>"ok",'data'=>$data),200);
        }

       

        $job->rates()->create([
            'client'=>auth('client')->user()->id,
            'rate'=>$rate,
        ]);
        $rate_count=$job->rates->sum('rate')==0 ? 0 : $job->rates->sum('rate') / $job->rates->count();


        $data=[
            'status'=>true,
            'rate'=>$rate_count
        ];

    return response()->json(array('msg'=>"ok",'data'=>$data),200);
    }

    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
