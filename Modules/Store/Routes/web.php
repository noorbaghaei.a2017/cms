<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/stores', 'StoreController');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/stores', 'StoreController@search')->name('search.store');
    });

    Route::group(["prefix"=>'store/categories'], function () {
        Route::get('/', 'StoreController@categories')->name('store.categories');
        Route::get('/create', 'StoreController@categoryCreate')->name('store.category.create');
        Route::post('/store', 'StoreController@categoryStore')->name('store.category.store');
        Route::get('/edit/{category}', 'StoreController@categoryEdit')->name('store.category.edit');
        Route::patch('/update/{category}', 'StoreController@categoryUpdate')->name('store.category.update');

    });

    Route::group(["prefix"=>'store/questions'], function () {
        Route::get('/{store}', 'StoreController@question')->name('store.questions');
        Route::get('/create/{store}', 'StoreController@questionCreate')->name('store.question.create');
        Route::post('/store/{store}', 'StoreController@questionStore')->name('store.question.store');
        Route::delete('/destroy/{question}', 'StoreController@questionDestroy')->name('store.question.destroy');
        Route::get('/edit/{store}/{question}', 'StoreController@questionEdit')->name('store.question.edit');
        Route::patch('/update/{question}', 'StoreController@questionUpdate')->name('store.question.update');
    });

});
