<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Modules\Advertising\Entities\Advertising;
use Modules\Advertising\Http\Requests\AdvertisingRequest;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\EmployerRequest;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Currency;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Helper\PlanHelper;
use Modules\Service\Http\Requests\AdvantageRequest;

class EmployerController extends Controller
{
    protected $entity;
    protected $advert;
    protected $class;
    private $repository;


//category

    protected $route_categories_index='client::categories.index';
    protected $route_categories_create='client::categories.create';
    protected $route_categories_edit='client::categories.edit';
    protected $route_categories='client.categories';


//question

    protected $route_questions_index='client::questions.index';
    protected $route_questions_create='client::questions.create';
    protected $route_questions_edit='client::questions.edit';
    protected $route_questions='clients.index';


//notification

    protected $notification_store='client::clients.store';
    protected $notification_update='client::clients.update';
    protected $notification_delete='client::clients.delete';
    protected $notification_error='client::clients.error';


    public function __construct()
    {
        $this->entity=new Client();
        $this->advert=new Advertising();

        $this->class=Client::class;

        $this->middleware('permission:employer-list')->only('index');
        $this->middleware('permission:employer-create')->only(['create','store']);
        $this->middleware('permission:employer-edit' )->only(['edit','update']);
        $this->middleware('permission:employer-delete')->only(['destroy']);
    }

    use  ResetsPasswords,HasCategory,HasQuestion;

    public function resetPass($object,$pass){

        $this->resetPassword($object,$pass);
    }
    public function resetPasswordProfile($object,$current,$pass){

        if(Hash::check($current, $object->password)){

            $this->resetPassword($object,$pass);

            return $errorStatus=true;
        }
        else{
            return $errorStatus=false;
        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity
                ->with('myCode','seo')
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })
                ->paginate(config('cms.paginate'));
            return view('client::employers.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Client::class)->get();
            return view('client::employers.create',compact('categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(EmployerRequest $request)
    {
        try {
            $this->entity->first_name=$request->input('firstname');
            $this->entity->last_name=$request->input('lastname');
            $this->entity->email=$request->input('email');
            $this->entity->mobile=$request->input('mobile');
            $this->entity->password=Hash::make($request->input('password'));
            $this->entity->identity_card=$request->input('identity_card');
            $this->entity->role=ClientRole::whereTitle('employer')->firstOrFail()->id;
            $this->entity->country=$request->input('country');
            $this->entity->phone=$request->input('phone');
            $this->entity->is_active=2;
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city_birthday=$request->input('city_birthday');
            $this->entity->address=$request->input('address');
            $this->entity->name=$request->input('name');
            $this->entity->token=tokenGenerate();

            $this->entity->save();


            $this->entity->analyzer()->create();

            $this->entity->myCode()->create([
                'code'=>generateCode()
            ]);


            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'private'=>$request->access,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,

            ]);

            $this->entity->company()->create([
                'name'=>$request->company_name,
                'count_member'=>$request->count_member,
                'mobile'=>$request->company_mobile,
                'phone'=>$request->company_phone,
                'website'=>$request->company_website,
                'address'=>$request->company_address,
                'country'=>$request->company_country,
                'state'=>$request->company_state,
                'city'=>$request->company_city,
                'area'=>$request->company_area,


            ]);

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->wallet()->create([
                'score'=>500
            ]);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if($request->has('logo')){
                $this->entity->addMedia($request->file('logo'))->toMediaCollection('logo');
            }
            if(!$this->entity->save()){
                return redirect()->back()->with('error',__('client::clients.error'));
            }else{
                return redirect(route("employers.index"))->with('message',__('client::employers.store'));

            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);

            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('client::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Client::class)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('client::employers.edit',compact('item','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(EmployerRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $update=$this->entity->update([
                "first_name"=>$request->input('firstname'),
                "last_name"=>$request->input('lastname'),
                "mobile"=>$request->input('mobile'),
                "email"=>$request->input('email'),
                "identity_card"=>$request->input('identity_card'),
                "postal_code"=>$request->input('postal_code'),
                "is_active"=>2,
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "phone"=>$request->input('phone'),
                "address"=>$request->input('address'),
                "city_birthday"=>$request->input('city_birthday'),
                "country"=>"Iran",
                "username"=>$request->input('username'),
                "name"=>$request->input('name'),
                "two_step"=>$request->input('two_step'),

            ]);

            $this->entity->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'private'=>$request->access,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,
            ]);

            $this->entity->company()->update([
                'name'=>$request->company_name,
                'count_member'=>$request->count_member,
                'mobile'=>$request->company_mobile,
                'website'=>$request->company_website,
                'phone'=>$request->company_phone,
                'address'=>$request->company_address,
                'country'=>$request->company_country,
                'state'=>$request->company_state,
                'city'=>$request->company_city,
                'area'=>$request->company_area,
            ]);

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if($request->has('logo')){
                destroyMedia($this->entity,'logo');
                $this->entity->addMedia($request->file('logo'))->toMediaCollection('logo');
            }

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){


                if(Hash::check($request->input('current_password'), $this->entity->password)){
                    $result=$this->entity->update([
                        'password'=>Hash::make($request->input('new_password'))
                    ]);
                    if(!$result){
                        return redirect()->back()->with('error',__('client::seekers.error-password'));
                    }

                }
                else{
                    return redirect()->back()->with('error',__('client::seekers.error-password'));


                }

            }

            if(!$update){
                return redirect()->back()->with('error',__('client::employers.error'));
            }else{
                return redirect(route("employers.index"))->with('message',__('client::clients.update'));

            }

        }catch (\Exception $exception){

            sendMailErrorController($exception);

            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function addAdvertising(Request $request,$client){
        try {
            $employer=Client::with('lastPlan')->has('lastPlan')->whereToken($client)->firstOrFail();
            $categories=Category::latest()->where('model',Advertising::class)->get();
            $currencies=Currency::latest()->get();
            return view('client::employers.advertisings.create',compact('categories','employer','plans','currencies'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public function storeAdvertising(Request $request,$employer){
        try {

            if(!hasPlan($employer)){
                return Redirect::back()->withErrors(['پلن شما منقضی شده است']);
            }
            else{


                $client=Client::whereToken($employer)->first();
                $category=Category::whereToken($request->input('category'))->first();
                $currency=Currency::whereToken($request->input('currency'))->first();

                $origin_plan=getPlan($employer)->lastPlan;
                $count=getPlan($employer)->lastPlan->count;

                if(Plan::findOrFail($origin_plan->id)->first()==$count){
                    return "true";
                }


                $this->advert->title=$request->input('title');
                $this->advert->excerpt=$request->input('excerpt');
                $this->advert->count_member=$request->input('count_member');
                $this->advert->number_employees=$request->input('number_employees');
                $this->advert->salary=$request->input('salary');
                $this->advert->work_experience=$request->input('experience');
                $this->advert->job_position=json_encode($request->input('positions'));
                $this->advert->skill=json_encode($request->input('skills'));
                $this->advert->status=$request->input('status');
                $this->advert->country=$request->input('country');
                $this->advert->education=json_encode($request->input('educations'));
                $this->advert->guild=$request->input('guild');
                $this->advert->kind=json_encode($request->input('kinds'));
                $this->advert->gender=$request->input('gender');
                $this->advert->text=$request->input('text');
                $this->advert->currency=$currency->id;
                $this->advert->plan=$employer->lastPlan->plan;
                $this->advert->expire=now()->addDays(Plan::find($employer->lastPlan->plan)->first()->time_limit);
                $this->advert->category=$category->id;
                $this->advert->client=$client->id;

                $this->advert->token=tokenGenerate();

                $this->advert->save();


                $this->advert->seo()->create([
                    'title'=>$request->input('title-seo'),
                    'description'=>$request->input('description-seo'),
                    'keyword'=>$request->input('keyword-seo'),
                    'canonical'=>$request->input('canonical-seo'),
                    'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                    'author'=>$request->input('author-seo'),
                    'publisher'=>$request->input('publisher-seo'),
                ]);



                $client->lastPlan()->update([
                    'count'=>$count -1
                ]);

                $this->advert->analyzer()->create();

                $this->advert->attachTags($request->input('tags'));

                if($request->has('image')){
                    $this->advert->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
                }
                return redirect(route("advertisings.index"))->with('message',__('advertising::advertisings.store'));

            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }
}
