<?php

namespace Modules\Widget\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Widget extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,TimeAttribute;

    protected $fillable = ['title','excerpt','text','category','slug','user','status','token','href','order'];


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(500)
            ->height(500)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(180)
            ->height(180)
            ->performOnCollections(config('cms.collection-image'));
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
