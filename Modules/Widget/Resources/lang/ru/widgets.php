<?php
return [
    "text-create"=>"you can create your widget",
    "text-edit"=>"you can edit your widget",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"widgets list",
    "error"=>"error",
    "singular"=>"widget",
    "collect"=>"widgets",
    "permission"=>[
        "widget-full-access"=>"widget full access",
        "widget-list"=>"widgets list",
        "widget-delete"=>"widget delete",
        "widget-create"=>"widget create",
        "widget-edit"=>"edit widget",
    ]


];
