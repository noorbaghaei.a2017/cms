@extends('core::dashboard.main')

@section('content')
   



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.edit')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.edit')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">


                @if(!$item->Hasmedia('images'))
                    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{asset('template/images/no-image.jpg')}}"
                       alt="" style="border-radius:unset;width:100%;">

    @else
    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{$item->getFirstMediaUrl('images')}}"
                       alt="" style="border-radius:unset;width:100%;">

    @endif
                 


                </div>

                <h3 class="profile-username text-center">{{$item->title}}</h3>

                <p class="text-muted text-center">Manager</p>

               

               

             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                 
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">{{__('cms.setting_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#privacy" data-toggle="tab">{{__('cms.privacy_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#translate" data-toggle="tab">{{__('cms.translate_lang')}}</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="tab-content">
                 
                 

                  <div class="tab-pane active" id="settings">
                    <form action="{{route('product.category.update', ['category' => $item->token])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PATCH')}}
                    <div class="form-group row">
                    <div class="col-sm-10">
                      <input name="image" type="file" onchange="loadFile(event)" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                    </div>  
                    
                    
                      <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">{{__('cms.title')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title" value="{{$item->title}}" class="form-control" id="title" placeholder="{{__('cms.title')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="icon" class="col-sm-2 col-form-label">{{__('cms.icon')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="icon" value="{{$item->icon}}" class="form-control" id="icon" placeholder="{{__('cms.icon')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="color" class="col-sm-2 col-form-label">{{__('cms.color')}}</label>
                        <div class="col-sm-10">
                          <input type="color" class="form-control" value="{{$item->color}}" name="color" id="color" placeholder="{{__('cms.color')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="pattern" class="col-sm-2 col-form-label">{{__('cms.pattern')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="pattern" value="{{$item->pattern}}" class="form-control" id="pattern" placeholder="{{__('cms.pattern')}}">
                        </div>
                      </div>
                     
                    
                      <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">{{__('cms.status')}}</label>
                        <div class="col-sm-10">
                        <select name="status" class="form-control">
                        <option  value="1" {{$item->status==1 ? 'selected' : ''}}>{{__('cms.active')}}</option>
                                        <option  value="0" {{$item->status==0 ? 'selected' : ''}}>{{__('cms.inactive')}}</option>
                        </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="parent" class="col-sm-2 col-form-label">{{__('cms.parent')}}</label>
                        <div class="col-sm-10">
                        <select name="parent" class="form-control">
                        <option  value="-1" {{$item->parent==0 ? 'selected' : ''}}>{{__('cms.self')}}</option>    

@foreach($parent_categories as $list)
                    <option value="{{$list->token}}" {{$item->parent==$list->id ? 'selected' : ''}}>{!! convert_lang($list,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                @endforeach
                        </select>
                        </div>
                      </div>

                   
                     

                      <div class="form-group row">
                        <label for="order" class="col-sm-2 col-form-label">{{__('cms.order')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="order" value="{{$item->order}}" class="form-control" id="order" placeholder="{{__('cms.order')}}">
                        </div>
                      </div>
                     
                    
                     
                   
                    
                     
                     
                     
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="privacy">
                   
                  </div>


                  <div class="tab-pane" id="translate">
                  <form action="{{route('category.product.language.update',['lang'=>'fa','token'=>$item->token])}}" method="POST">
                  @csrf
                            @method('PATCH')
                  <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.title_fa')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title" value="{{isset($item->translates->where('lang','fa')->first()->title) ? $item->translates->where('lang','fa')->first()->title : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.title_fa')}}" autocomplete="off">
                        </div>
                      </div>
                    
                      
                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>

                      </form>
                      <form action="{{route('category.product.language.update',['lang'=>'en','token'=>$item->token])}}" method="POST">
                  @csrf
                  @method('PATCH')
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.title_en')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title" value="{{isset($item->translates->where('lang','en')->first()->title) ? $item->translates->where('lang','en')->first()->title : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.title_en')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>

                      </form>

                   </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>





@endsection


@section('scripts')


<script>

function loadFile(e) {
	 image = $('#show-image');
    
    image.attr('src',URL.createObjectURL(event.target.files[0]));
    
};


</script>

@endsection
 
 
 
