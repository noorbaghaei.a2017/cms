<?php

namespace Modules\Product\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\Product;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Product::class)->delete();

        Permission::create(['name'=>'product-list','model'=>Product::class,'created_at'=>now()]);
        Permission::create(['name'=>'product-create','model'=>Product::class,'created_at'=>now()]);
        Permission::create(['name'=>'product-edit','model'=>Product::class,'created_at'=>now()]);
        Permission::create(['name'=>'product-delete','model'=>Product::class,'created_at'=>now()]);
    }
}
