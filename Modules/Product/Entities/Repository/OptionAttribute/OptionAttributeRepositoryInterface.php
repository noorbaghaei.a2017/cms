<?php


namespace Modules\Product\Entities\Repository\OptionAttribute;


interface OptionAttributeRepositoryInterface
{
    public function getAll();
    public function getOption($token);

}
