<?php
return [
    "text-create"=>"you can create your Bewertungen",
    "text-edit"=>"you can edit your Bewertungen",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"Bewertungen list",
    "singular"=>"Bewertungen",
    "collect"=>"Bewertungen",
    "error"=>"exception to send Bewertungen try again",
    "permission"=>[
        "questions-full-access"=>"Bewertungen full access",
        "questions-list"=>"Bewertungen list",
        "questions-delete"=>"Bewertungen delete",
        "questions-create"=>"Bewertungen create",
        "questions-edit"=>"edit Bewertungen",
    ]
];
