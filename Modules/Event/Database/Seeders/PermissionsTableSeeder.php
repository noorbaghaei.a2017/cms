<?php

namespace Modules\Event\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Event\Entities\Event;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Event::class)->delete();

        Permission::create(['name'=>'event-list','model'=>Event::class,'created_at'=>now()]);
        Permission::create(['name'=>'event-create','model'=>Event::class,'created_at'=>now()]);
        Permission::create(['name'=>'event-edit','model'=>Event::class,'created_at'=>now()]);
        Permission::create(['name'=>'event-delete','model'=>Event::class,'created_at'=>now()]);
    }
}
