<?php

namespace Modules\Event\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Event\Entities\Event;
use Modules\Event\Entities\Repository\EventRepositoryInterface;
use Modules\Event\Http\Requests\EventRequest;
use Modules\Event\Transformers\EventCollection;

class EventController extends Controller
{

    use HasQuestion,HasCategory,HasGallery;

    protected $entity;

    protected $class;

    private $repository;

//category

    protected $route_categories_index='event::categories.index';
    protected $route_categories_create='event::categories.create';
    protected $route_categories_edit='event::categories.edit';
    protected $route_categories='event.categories';

//question

    protected $route_questions_index='event::questions.index';
    protected $route_questions_create='event::questions.create';
    protected $route_questions_edit='event::questions.edit';
    protected $route_questions='events.index';


//gallery

    protected $route_gallery_index='event::events.gallery';
    protected $route_gallery='events.index';



//notification

    protected $notification_store='event::events.store';
    protected $notification_update='event::events.update';
    protected $notification_delete='event::events.delete';
    protected $notification_error='event::events.error';


    public function __construct(EventRepositoryInterface $repository)
    {
        $this->entity=new Event();

        $this->class=Event::class;

        $this->repository=$repository;

        $this->middleware('permission:event-list');
        $this->middleware('permission:event-create')->only(['create','store']);
        $this->middleware('permission:event-edit' )->only(['edit','update']);
        $this->middleware('permission:event-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $events = new EventCollection($items);

            $data= collect($events->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Event::class)->get();
            return view('event::events.create',compact('categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title) &&
            !isset($request->slug)
            ){
                $items=$this->repository->getAll();
                $result = new EventCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("slug",'LIKE','%'.trim($request->slug).'%')
                ->paginate(config('cms.paginate'));
            $result = new EventCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(EventRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->start_at=convertJalali($request->date_start,$request->time_start);
            $this->entity->end_at=convertJalali($request->date_end,$request->time_end);
            $this->entity->sign_start_at=convertJalali($request->sign_date_start,$request->sign_time_start);
            $this->entity->sign_end_at=convertJalali($request->sign_date_end,$request->sign_time_end);
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->country=$request->input('country');
            $this->entity->refer_link=$request->input('refer_link');
            $this->entity->address=$request->input('address');
            $this->entity->longitude=$request->input('longitude');
            $this->entity->latitude=$request->input('latitude');
            $this->entity->google_map=$request->input('google_map');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->week()->create([
                'saturday'=>$request->input('saturday'),
                'sunday'=>$request->input('sunday'),
                'monday'=>$request->input('monday'),
                'tuesday'=>$request->input('tuesday'),
                'wednesday'=>$request->input('wednesday'),
                'thursday'=>$request->input('thursday'),
                'friday'=>$request->input('friday'),
            ]);
            $this->entity->analyzer()->create();

            $this->entity->attachTags($request->input('tags'));
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$saved){
                return redirect()->back()->with('error',__('event::events.error'));
            }else{
                return redirect(route("events.index"))->with('message',__('event::events.store'));
            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Event::class)->get();
            $item=$this->entity->with('week')->whereToken($token)->first();
            return view('event::events.edit',compact('item','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Update the specified resource in storage.
     * @param EventRequest $request
     * @param $token
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {

            $validator=Validator::make($request->all(),[
                'title'=>'required|unique:events,title,'.$token.',token',
                'date_end'=>'required',
                'date_start'=>'required',
                'time_start'=>'required|regex:/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]+$/',
                'time_end'=>'required|regex:/^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]+$/',
                'text'=>'required',
                'excerpt'=>'required'
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "start_at"=>convertJalali($request->input('date_start'),$request->input('time_start')),
                "end_at"=>convertJalali($request->input('date_end'),$request->input('time_end')),
                "sign_start_at"=>convertJalali($request->input('sign_date_start'),$request->input('sign_time_start')),
                "sign_end_at"=>convertJalali($request->input('sign_date_end'),$request->input('sign_time_end')),
                "excerpt"=>$request->input('excerpt'),
                "refer_link"=>$request->input('refer_link'),
                "country"=>$request->input('country'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "address"=>$request->input('address'),
                "longitude"=>$request->input('longitude'),
                "latitude"=>$request->input('latitude'),
                "google_map"=>$request->input('google_map'),
                "text"=>$request->input('text'),
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->week()->update([
                'saturday'=>$request->input('saturday'),
                'sunday'=>$request->input('sunday'),
                'monday'=>$request->input('monday'),
                'tuesday'=>$request->input('tuesday'),
                'wednesday'=>$request->input('wednesday'),
                'thursday'=>$request->input('thursday'),
                'friday'=>$request->input('friday'),
            ]);

            $this->entity->syncTags($request->input('tags'));

            if(!$updated){
                return redirect()->back()->with('error',__('event::events.error'));
            }else{
                return redirect(route("events.index"))->with('message',__('event::events.update'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $this->entity->seo()->delete();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('event::events.error'));
            }else{
                return redirect(route("events.index"))->with('message',__('event::events.delete'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


}
