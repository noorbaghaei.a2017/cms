<?php
return [
    "text-create"=>"you can create your plan",
    "text-edit"=>"you can edit your plan",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"plans list",
    "singular"=>"plan",
    "collect"=>"plans",
    "permission"=>[
        "plan-full-access"=>"plans full access",
        "plan-list"=>"plans list",
        "plan-delete"=>"plan delete",
        "plan-create"=>"plan create",
        "plan-edit"=>"edit plan",
    ]
];
