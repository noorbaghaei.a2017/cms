<?php


namespace Modules\Educational\Entities\Repository;


use Modules\Educational\Entities\ClassRoom;

class ClassRoomRepository implements ClassRoomRepositoryInterface
{

    public function getAll()
    {
       return ClassRoom::latest()->whereParent(0)->get();

    }
}
