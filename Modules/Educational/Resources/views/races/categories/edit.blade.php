@include('core::layout.modules.category.edit',[

    'title'=>__('core::categories.create'),
    'item'=>$item,
    'parent'=>'educational',
    'model'=>'race',
    'directory'=>'race',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'update_route'=>['name'=>'race.category.update','param'=>'category'],

])








