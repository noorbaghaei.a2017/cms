@include('core::layout.modules.index',[

    'title'=>__('service::properties.index'),
    'items'=>$items,
    'parent'=>'service',
    'model'=>'property',
    'language_route'=>true,
    'class_model'=> 'property',
    'directory'=>'properties',
    'collect'=>__('service::properties.collect'),
    'singular'=>__('service::properties.singular'),
    'create_route'=>['name'=>'properties.create'],
    'edit_route'=>['name'=>'properties.edit','name_param'=>'property'],
    'destroy_route'=>['name'=>'properties.destroy','name_param'=>'property'],
     'search_route'=>true,
     'setting_route'=>true,
     'pagination'=>true,
    'datatable'=>[
        __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
        __('cms.status')=>'GetStatus',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
         __('cms.status')=>'GetStatus',
     __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
