<?php

namespace Modules\Agent\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Modules\Core\Entities\Info;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\Wallet;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Agent extends Model implements HasMedia
{
    use HasApiTokens,HasTags,HasMediaTrait,Sluggable,TimeAttribute,Notifiable;

    protected  $table='agents';

    protected $fillable = [
        'first_name',
        'last_name',
        'password',
        'country',
        'order',
        'branch',
        'token',
        'slug',
        'text',
        'mobile',
        'email',
        'user'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function wallet()
    {
        return $this->morphOne(Wallet::class, 'walletable');
    }

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }

    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }



    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'last_name'
            ]
        ];
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

    }

    public  function getFullNameAttribute(){
        return $this->first_name." ".$this->last_name;
    }
}
