@include('core::layout.modules.category.create',[

    'title'=>__('core::categories.create'),
    'parent'=>'article',
    'model'=>'article',
    'directory'=>'articles',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'store_route'=>['name'=>'article.category.store'],

])








