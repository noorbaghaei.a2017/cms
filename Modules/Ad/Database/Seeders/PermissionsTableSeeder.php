<?php

namespace Modules\Ad\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Ad\Entities\Ad;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->whereModel(Ad::class)->delete();

        Permission::create(['name'=>'ad-list','model'=>Ad::class,'created_at'=>now()]);
        Permission::create(['name'=>'ad-create','model'=>Ad::class,'created_at'=>now()]);
        Permission::create(['name'=>'ad-edit','model'=>Ad::class,'created_at'=>now()]);
        Permission::create(['name'=>'ad-delete','model'=>Ad::class,'created_at'=>now()]);

    }
}
