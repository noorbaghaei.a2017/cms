<?php

return [
    'name' => 'Advertising',
    'icons'=>[
        'advertising'=>'ion-ios-book',
        'job'=>'ion-ios-book',
        'category'=>'fa fa-list-alt',
        'menu'=>'fa fa-menu',
        'attribute'=>'fa fa-info',
        'skill'=>'ion-ios-book',
        'position'=>'ion-ios-book',
        'salary'=>'ion-ios-book',
        'type'=>'ion-ios-book',
        'experience'=>'ion-ios-book',
        'education'=>'ion-ios-book',
        'guild'=>'ion-ios-book',
        'period'=>'ion-ios-book',
    ]
];
