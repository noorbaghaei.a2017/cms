<?php

namespace Modules\Advertising\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Advertising\Http\Requests\JobStoreRequest;
use Modules\Advertising\Http\Requests\JobUpdateRequest;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\UserServices;
use Modules\Core\Entities\ListServices;
use Modules\Advertising\Entities\MenuJob;
use Modules\Client\Entities\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Maatwebsite\Excel\Facades\Excel;
use App\Imports\JobImport;

class JobController extends Controller
{

    protected $entity;
    protected $class;

    public function __construct()
    {
        $this->entity=new UserServices();
        $this->class=UserServices::class;
      
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {

            $items=UserServices::with('user_info')->latest()->get();
            return view('advertising::jobs.index',compact('items'));

        }catch (\Exception $exception){
           
            return abort('500');
        }
    }
      /**
     * Display a listing of the resource.
     * @return Response
     */
    public function new()
    {
        try {

            $items=UserServices::with('user_info')->where('status',1)->latest()->get();

            return view('advertising::jobs.index',compact('items'));

        }catch (\Exception $exception){
           
            return abort('500');
        }
    }
      /**
     * Display a listing of the resource.
     * @return Response
     */
    public function reserve()
    {
        try {

            $items=UserServices::with('user_info')->where('user',null)->latest()->get();

            return view('advertising::jobs.index',compact('items'));

        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

    
    public function approved($token)
    {
        try {
            DB::beginTransaction();
            $item=UserServices::where('token',$token)->firstOrFail();
            $client=Client::find($item->user);

            if($item->status!="2"){

                $item->update([
                    'status'=>2
                ]);
    
                
                $result=[
                     
                    'email'=>$client->email,
                    'firstname'=>$client->first_name,
                    'title'=>'Zugelassen',
    
                ];
    
                sendNotificationCustomEmail($result,'emails.front.approve-job'); 
            
                DB::commit();
                return redirect()->back()->with('message',__('advertising::jobs.store'));
            }
            else{
                DB::rollBack();
           
                return redirect()->back()->with('error',__('advertising::jobs.error'));
            }
            


        }catch (\Exception $exception){
            
            
            DB::rollBack();
           
            return redirect()->back()->with('error',__('advertising::jobs.error'));

        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $clients=Client::latest()->get();
            $list_services=ListServices::whereParent(0)->get();
            return view('advertising::jobs.create',compact('clients','list_services'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(JobStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->address=$request->input('address');
            $this->entity->phone=$request->input('phone');
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city=$request->input('city');
            $this->entity->service=$request->has('sub_category') ?$request->sub_category : $request->category;
            $this->entity->status=2;
            $this->entity->is_special=$request->input('is_special');
            $this->entity->country='german';
            $this->entity->google_map='';
            $this->entity->longitude='';
            $this->entity->latitude='';
            $this->entity->email=$request->input('email');
            $this->entity->user=$request->client=='0' ? null : Client::whereToken($request->input('client'))->first()->id;
            $this->entity->text=$request->input('text');
            $this->entity->token=tokenResetGenerate();


            $saved=$this->entity->save();

         
            $this->entity->info()->create();
                 
                $this->entity->analyzer()->create();

                $this->entity->time()->create([
                    'token'=>tokenResetGenerate()
                ]);
          
            
            if($request->input('title_fa')){
                $this->entity->translates()->create([
                    'title'=>$request->input('title_fa'),
                    'lang'=>'fa'
                ]);
            }
            if($request->input('title_en')){
                $this->entity->translates()->create([
                    'title'=>$request->input('title_en'),
                    'lang'=>'en'
                ]);
            }
          
           

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::jobs.error'));
            }else{
                DB::commit();
                return redirect(route("jobs.index"))->with('message',__('advertising::jobs.store'));
            }

           
        }catch (Exception $exception){
            DB::rollBack();

          
            return redirect()->back()->with('error',__('advertising::jobs.error'));

        }

    }

    

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('advertising::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
           
            $item=UserServices::whereToken($token)->first();
            $menus=MenuJob::latest()->get();
            $clients=Client::latest()->get();
            $list_services=ListServices::whereParent(0)->get();
            $service=ListServices::find($item->service);
            $parent=ListServices::find($service->parent);
            $childs=ListServices::whereParent($parent->id)->get();
            return view('advertising::jobs.edit',compact('item','menus','list_services','parent','childs','service','clients'));
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('advertising::jobs.error'));

        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(JobUpdateRequest $request, $token)
    {
        try {
            DB::beginTransaction();
            $this->entity=UserServices::whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
               
                "slug"=>null,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "user"=>$request->client=='0' ? null : Client::whereToken($request->input('client'))->first()->id,
                "service"=>$request->has('sub_category') ?$request->sub_category : $request->category,
                "email"=>$request->input('email'),
                "is_special"=>$request->input('is_special'),
                "phone"=>$request->input('phone'),
                "address"=>$request->input('address'),
                "postal_code"=>$request->input('postal_code'),
                "city"=>$request->input('city'),
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

           

            
            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::jobs.error'));
            }else{
                DB::commit();
                return redirect(route("jobs.index"))->with('message',__('advertising::jobs.update'));
            }


        }catch (\Exception $exception){
          
            DB::rollBack();
          
            return redirect()->back()->with('error',__('advertising::jobs.error'));

        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public  function languageShow(Request $request,$lang,$token){
        $item=UserServices::with('translates')->where('token',$token)->first();
        return view('advertising::jobs.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=UserServices::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt
            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('advertising::job.error'));
        }else{
            DB::commit();
            return redirect(route("jobs.index"))->with('message',__('advertising::jobs.update'));
        }

    }

    public function importFile(Request $request){

        try{
            Excel::import(new JobImport,$request->file);
            return redirect(route("jobs.index"))->with('message',__('advertising::jobs.update'));
        }catch(\Exception $exception){
            return redirect()->back()->with('error',__('advertising::job.error'));
        }
       
       
    }
    public  function TimeUpdate(Request $request,$token){
        try{
        DB::beginTransaction();
        $item=UserServices::with('time')->whereToken($token)->firstOrFail();

      
        if(is_null($item->time)){
            $item->time()->create([
                'client'=>auth('client')->user()->id,
                'monday'=>$request->monday,
                'tuesday'=>$request->tuesday,
                'wednesday'=>$request->wednesday,
                'thursday'=>$request->thursday,
                'friday'=>$request->friday,
                'saturday'=>$request->saturday,
                'sunday'=>$request->sunday,
                'token'=>tokenGenerate()
            ]);
        }
        else{
            $item->time()->update([
                'monday'=>$request->monday,
                'tuesday'=>$request->tuesday,
                'wednesday'=>$request->wednesday,
                'thursday'=>$request->thursday,
                'friday'=>$request->friday,
                'saturday'=>$request->saturday,
                'sunday'=>$request->sunday,

            ]);
        }
        DB::commit();
        return redirect()->back()->with('message',__('advertising::webung.update'));
    
    }catch(\Exception $exception){
        
        DB::rollBack();
        return redirect()->back()->with('error',__('advertising::werbung.error'));
    }

     

    }

    public  function BannerUpdate(Request $request,$token){
        try{
        DB::beginTransaction();
        $data=UserServices::with('time')->whereToken($token)->firstOrFail();

        if($request->has('banner')){
            destroyMedia($data,'banner');
            $data->addMedia($request->file('banner'))->toMediaCollection('banner');
                            
        }
  

        DB::commit();
        return redirect()->back()->with('message',__('advertising::webung.update'));
    
    }catch(\Exception $exception){
        DB::rollBack();
        return redirect()->back()->with('error',__('advertising::werbung.error'));
    }

     

    }

    public  function AddGallery(Request $request,$token){
        try{
        DB::beginTransaction();
        $data=UserServices::with('time')->whereToken($token)->firstOrFail();

        if($request->has('photo')){
        
            $data->addMedia($request->file('photo'))->toMediaCollection(config('cms.collection-images'));
                            
        }
  

        DB::commit();
        return redirect()->back()->with('message',__('advertising::webung.update'));
    
    }catch(\Exception $exception){
        DB::rollBack();
        return redirect()->back()->with('error',__('advertising::werbung.error'));
    }

     

    }

    public  function StoreMenuJob(Request $request,$menu,$token){
        try{
        DB::beginTransaction();

        $item=UserServices::with('menus')->whereToken($token)->firstOrFail();
        
        
        $data=$item->menus()->create([
            'client'=>$item->client,
            'menu'=>$menu,
            'service'=>$item->service,
            'title'=>$request->title,
            'excerpt'=>$request->excerpt,
            'price'=>$request->price,
            'token'=>tokenGenerate()
        ]);

      

            if($request->has("image")){
            
                $data->addMedia($request->file("image"))->toMediaCollection('images');

            }
  

        DB::commit();
        return redirect()->back()->with('message',__('advertising::webung.update'));
    
    }catch(\Exception $exception){
      
        DB::rollBack();
        return redirect()->back()->with('error',__('advertising::werbung.error'));
    }

     

    }
    
}
