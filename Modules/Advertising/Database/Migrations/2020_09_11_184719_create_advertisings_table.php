<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user')->unsigned()->nullable();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('client')->unsigned()->nullable();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->unsignedBigInteger('category')->unsigned()->nullable();
            $table->foreign('category')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('plan')->unsigned()->nullable();
            $table->foreign('plan')->references('id')->on('plans')->onDelete('cascade');
            $table->string('title');
            $table->string('salary')->nullable();
            $table->string('type_salary')->nullable();
            $table->tinyInteger('special')->default(0)->nullable();
            $table->tinyInteger('access_mobile')->default(0);
            $table->tinyInteger('access_phone')->default(0);
            $table->tinyInteger('access_address')->default(0);
            $table->tinyInteger('access_email')->default(0);
            $table->tinyInteger('force')->nullable();
            $table->string('gender')->nullable();
            $table->string('number_employees')->nullable();
            $table->string('phone')->nullable();
            $table->string('type');
            $table->string('email');
            $table->string('postal_code');
            $table->text('address')->nullable();
            $table->string('mobile')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city');
            $table->string('currency')->nullable();
            $table->tinyInteger('display')->nullable();
            $table->json('education')->nullable();
            $table->string('guild')->nullable();
            $table->json('kind')->nullable();
            $table->string('work_experience')->nullable();
            $table->json('job_position')->nullable();
            $table->json('skill')->nullable();
            $table->string('slug');
            $table->longText('text');
            $table->tinyInteger('status')->default(1);
            $table->string('excerpt')->nullable();
            $table->timestamp('expire')->nullable();
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisings');
    }
}
