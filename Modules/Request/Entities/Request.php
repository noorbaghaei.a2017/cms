<?php

namespace Modules\Request\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Request extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = ['email','title','ip','name','phone','mobile','gender','list_requests','country','text','loss','color','fall_time','transplantation','feeling','execution_time'];


//    public  function getVorneAttribute(){
//
//        return  $this->Hasmedia('Vorne') ? asset($this->getFirstMediaUrl('Vorne')) :asset('img/no-img.gif');
//
//    } public  function getHintenAttribute(){
//
//    return  $this->Hasmedia('Hinten') ? asset($this->getFirstMediaUrl('Hinten')) :asset('img/no-img.gif');
//
//    }
//    public  function getObenAttribute(){
//
//        return  $this->Hasmedia('Oben') ? asset($this->getFirstMediaUrl('Oben')) :asset('img/no-img.gif');
//    }


}
