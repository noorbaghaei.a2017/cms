<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberBotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_bot', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member')->unsigned();;
            $table->foreign('member')->references('id')->on('members')->onDelete('cascade');
            $table->string('count')->default(1);
            $table->text('description')->nullable();
            $table->string('title')->nullable();
            $table->morphs('botable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_bot');
    }
}
