<?php

namespace Modules\Member\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Member\Entities\Member;

class ManageMemberController extends Controller
{
    public function panel(){

        try {
            $member=Member::find(\auth('member')->user()->id);
            return view('member::admin.dashboard',compact('member'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');

        }
    }


}
