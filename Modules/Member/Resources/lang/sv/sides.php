<?php
return [
    "text-create"=>"you can create your sides",
    "text-edit"=>"you can edit your sides",
    "store"=>"Store Success",
    "update"=>"Update Success",
    "index"=>"sides list",
    "singular"=>"sides",
    "collect"=>"sides",
    "permission"=>[
        "sides-full-access"=>"sides full access",
        "sides-list"=>"sides list",
        "sides-create"=>"sides create",
        "sides-edit"=>"edit sides",
    ]
];
