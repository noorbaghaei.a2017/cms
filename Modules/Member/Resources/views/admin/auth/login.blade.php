@include('member::admin.auth.sections.header')

<body class="theme-blush">

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <form action="{{route('member.login.submit')}}" method="POST" class="card auth_form">
                    @csrf
                    <div class="header">
                        @if(!$setting->Hasmedia('logo'))
                            <img class="logo" src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}" title="{{$setting->name}}" width="80">
                        @else
                            <img class="logo" src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" title="{{$setting->name}}" width="80">
                        @endif

                        <h5>پنل کارمندان</h5>
                    </div>
                    <div class="body text-center">
                        @error('mobile')
                        <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        @error('email')
                        <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        @error('username')
                        <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="input-group mb-3">
                            <input type="text" name="identify" class="form-control text-right" placeholder="ایمیل" autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control text-right" placeholder="کلمه عبور" autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-key"></i></span>
                            </div>
                        </div>

                        <input type="submit" class="btn btn-primary btn-block waves-effect waves-light" value="ورود">

                    </div>
                </form>
                <div class="copyright text-center">
                    &copy;
                    <span><a href="#">amin nourbaghaei</a></span>
                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src="{{asset('member/images/signin.svg')}}" alt="Sign In"/>
                </div>
            </div>
        </div>
    </div>
</div>

@include('member::admin.auth.sections.footer')



</html>
