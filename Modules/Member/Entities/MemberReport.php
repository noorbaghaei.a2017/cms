<?php

namespace Modules\Member\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberReport extends Model
{
    protected $table = 'member_reports';
    protected $fillable = ['member','text','range','start','end'];
    
}
