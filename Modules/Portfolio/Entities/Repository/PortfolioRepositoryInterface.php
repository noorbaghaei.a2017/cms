<?php


namespace Modules\Portfolio\Entities\Repository;


interface PortfolioRepositoryInterface
{
    public function getAll();

}
