@extends('core::layout.panel')
@section('pageTitle', $data['title'])
@section('content')
    <div class="padding">

        @include('core::layout.alert-success')
     


        <div class="row">
            <div class="col-sm-12">

                <div class="box">

                    <div class="box-header">
                        <div class="pull-left">
                            <h2>{{$data['collect']}}</h2>
                            <small>{{$data['collect']}}</small>
                        </div>
                        <div class="text-center">
                        @if(Request::isMethod('get'))

                            @if($data['count']!=0)
                                <h6>{{$data['count']}} مورد </h6>
                            @else
                                <h6>موردی یافت نشد . </h6>
                                @endif

                         @elseif(Request::isMethod('post'))

                                    @if($data['count']!=0)
                                        <h6>{{$data['count']}} مورد </h6>
                                    @else
                                        <h6>موردی یافت نشد . </h6>
                                    @endif

                            @endif

                        </div>



                            @foreach($data['public_route'] as $public)

                                @if(is_null($public->param))
                               <a title="{{$public->title}}" href="{{route($public->name)}}" class="{{$public->class}}"><span class="{{$public->icon}}"></span>  </a>

                            @else
                                <a title="{{$public->title}}" href="{{route($public->name,convertArrayParam(collect($public->param)->toArray()))}}" class="{{$public->class}}"><span class="{{$public->icon}}"></span>  </a>

                            @endif
                            @endforeach



                    </div>
                    <div class="table-responsive">

                        <table class="table table-striped b-t">
                            <thead>
                            <tr>

                                @foreach($data['filed'] as $key=>$value)
                                    <th>{{__('cms.'.$value)}}</th>
                                @endforeach
                                    @if(isset($data['language_route']))
                                    <th>{{__('cms.language')}}</th>
                                    @endif
                                <th>{{__('cms.options')}}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['data'] as $key=>$values)
                                <tr>
                                   @foreach($values as $name=>$value)

                                            @if(in_array($name,$data['filed']))

                                                    @if($name=='thumbnail')
                                                        <td>
                                                            <img style="width: 80px;height: auto" src="{{$values->$name}}" alt="" class="img-responsive" >
                                                        </td>
                                                    @else
                                                        <td>
                                                            {!! $values->$name !!}
                                                        </td>
                                                    @endif

                                            @endif



                                   @endforeach
                                    @if(isset($data['language_route']))
                                       <td>
                                           @foreach(get_language($values->token,$data['class_model']) as $language)
                                               <a href="{{route($data['class_model'].'.language.show',['lang'=>$language->lang,'token'=>$values->token])}}">
                                                   <img src="{{asset(config('cms.flag.'.$language->lang))}}" width="20">
                                               </a>
                                           @endforeach
                                       </td>
                                       @endif
                                    <td>
                                    @foreach($data['private_route'] as $private)

                                           @if($private->modal)


                                               @if($private->modal_type=='destroy')

                                                    @include('core::response.modal.destroy',compact('values','$private'))

                                                @elseif($private->modal_type=='detail')

                                                   @include('core::response.modal.detail',compact('values','$private'))

                                                @endif


                                            @else

                                                <a title="{{$private->title}}" href="{{route($private->name, convertArrayParam(collect($private->param)->toArray(),$values))}}" class="{{$private->class}}"><span class="{{$private->icon}}"></span></a>


                                            @endif




                                    @endforeach
                                    </td>

                                </tr>


                            @endforeach
                            </tbody>
                        </table>





                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script>
    $('ul.pagination').hide();
    $(function() {
        $('.scrolling-pagination').jscroll({
            autoTrigger: true,
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.scrolling-pagination',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script>
@endsection
