@extends('core::dashboard.main')

@section('content')
   



@can('user-list')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">{{__('cms.dashboard') }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('cms.dashboard')}}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <h5 class="mb-2">{{__('cms.new')}}</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><a href="#"><i class="far fa-envelope"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.ticket')}}</span>
                <span class="info-box-number">{{$requestscount}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><a href="{{route('new.index.products')}}"><i class="fas fa-shopping-cart"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.product')}}</span>
                <span class="info-box-number">{{$new_products_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><a href="{{route('new.index.jobs')}}"><i class="fas fa-suitcase"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.jobs')}}</span>
                <span class="info-box-number">{{$new_jobs_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><a href="{{route('new.index.advertisings')}}"><i class="fas fa-newspaper"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.advertisings')}}</span>
                <span class="info-box-number">{{$new_advertisings_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><a href="{{route('new.index.clients')}}"><i class="fas fa-users"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.clients')}}</span>
                <span class="info-box-number">{{$new_clients_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->


        <h5 class="mb-2">{{__('cms.reserve')}}</h5>
        <div class="row">
         
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><a href="{{route('reserve.index.products')}}"><i class="fas fa-shopping-cart"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.product')}}</span>
                <span class="info-box-number">{{$reserve_products_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><a href="{{route('reserve.index.jobs')}}"><i class="fas fa-suitcase"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.jobs')}}</span>
                <span class="info-box-number">{{$reserve_jobs_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><a href="{{route('reserve.index.advertisings')}}"><i class="fas fa-newspaper"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.advertisings')}}</span>
                <span class="info-box-number">{{$reserve_advertisings_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

     
        <!-- =========================================================== -->
        <h5 class="mt-4 mb-2">{{__('cms.percent')}}</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-info">
              <span class="info-box-icon"><i class="far fa-eye"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">View</span>
                <span class="info-box-number">0</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 0%"></div>
                </div>
                <span class="progress-description">
                  0% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-success">
              <span class="info-box-icon"><i class="far fa-thumbs-up"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Likes</span>
                <span class="info-box-number">0</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 0%"></div>
                </div>
                <span class="progress-description">
                  0% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-warning">
              <span class="info-box-icon"><i class="fas fa-server"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">IP</span>
                <span class="info-box-number">{{$ipviewscount}}</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 0%"></div>
                </div>
                <span class="progress-description">
                  0% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-danger">
              <span class="info-box-icon"><i class="fas fa-comments"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Comments</span>
                <span class="info-box-number">0</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 0%"></div>
                </div>
                <span class="progress-description">
                  0% Increase in 30 Days
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->


      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <h5 class="mt-4 mb-2">{{__('cms.all_information')}}</h5>
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>0</h3>

                <p>{{__('cms.order')}}</p>
              </div>
              <div class="icon">
                <i class="fa fa-th-list"></i>
              </div>
              <a href="#" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$productscount}}</h3>

                <p>{{__('cms.products')}}</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <a href="{{route('products.index')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$categoriesproductcount}}</h3>

                <p>{{__('cms.category-products')}}</p>
              </div>
              <div class="icon">
                <i class="fa fa-th-large"></i>
              </div>
              <a href="{{route('product.categories')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
            <!-- ./col -->
            <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$categoriescount}}</h3>

                <p>{{__('cms.category-bussiness')}}</p>
              </div>
              <div class="icon">
                <i class="fa fa-th-large"></i>
              </div>
              <a href="{{route('categoryjobs.index')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$clientscount}}</h3>

                <p>{{__('client::clients.collect')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{route('clients.index')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

            <!-- ./col -->
            <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$all_bussiness_special_count}}</h3>

                <p>{{__('cms.special')}}</p>
              </div>
              <div class="icon">
                <i class="fa fa-gift"></i>
              </div>
              <a href="{{route('jobs.index')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$bussinesscount}}</h3>

                <p>{{__('cms.bussiness')}}</p>
              </div>
              <div class="icon">
                <i class="fas fa-suitcase"></i>
              </div>
              <a href="{{route('jobs.index')}}" class="small-box-footer">{{__('cms.read_more')}} <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

</div>
<h5 class="mt-4 mb-2">{{__('cms.chart')}}</h5>
<div class="row">
  <!-- DONUT CHART -->
  <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.jobs')}}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
        
       
    <h5 class="mb-2">{{__('cms.best-advertisings')}}</h5>
        <div class="card card-success">
          <div class="card-body">


            <div class="row">

            @foreach($popular_bussiness as $top_businness)
              <div class="col-md-3 col-lg-12 col-xl-3">
                <div class="card mb-2 bg-gradient-dark">

                @if($top_businness->Hasmedia('images'))
                <img class="card-img-top" src="{{$top_businness->getFirstMediaUrl('images')}}" alt="Dist Photo 1">

                @elseif(findOriginCategoryJob($top_businness->service)->Hasmedia('images'))
                                                                    
                <img class="card-img-top" src="{{findOriginCategoryJob($top_businness->service)->getFirstMediaUrl('images')}}" alt="Dist Photo 1">

                                                                    @else
                                                                    <img class="card-img-top" src="{{asset('template/images/no-image.jpg')}}" alt="Dist Photo 1">

   
        
    @endif


                  <div class="card-img-overlay d-flex flex-column justify-content-end">
                    <h5 class="card-title text-primary text-white">{{$top_businness->title}}</h5>
                    <a href="#" class="text-white"></a>
                  </div>
                </div>
              </div>
              @endforeach
           
            </div>
          </div>
        </div>


        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    
@endcan

@endsection

@section('scripts')

<script src="{{asset('admin/js/plugins/chart.js/Chart.min.js')}}"></script>

<script>

$(function () {


  var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Berlin',
          'Köln',
          'Hamburg',
          'Frankfurt am Main',
          'München',
         
      ],
      datasets: [
        {
          data: [0,17,1,0,0],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })



})

</script>

@endsection