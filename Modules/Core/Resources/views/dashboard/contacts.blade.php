@extends('core::dashboard.main')

@section('content')
   


 <!-- Content Header (Page header) -->
 <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> {{__('cms.manager')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.contacts')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <!-- /.container-fluid -->

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row">
              @foreach($admins as $admin)
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
              <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                {{__('cms.manager')}}
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-7">
                      <h2 class="lead"><b>{{$admin->full_name}}</b></h2>
                      <p class="text-muted text-sm"><b>{{__('cms.about')}}: </b> </p>
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> {{__('cms.address')}}: {{$admin->address}}</li>
                        <li class="small"><span class="fa-li"><i class="far fa-envelope"></i></span> {{__('cms.email')}}: {{$admin->email}}</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> {{__('cms.phone')}} #:{{$admin->mobile}}</li>
                      </ul>
                    </div>
                    <div class="col-5 text-center">
                    @if(!$admin->Hasmedia('images'))
                    <img src="{{asset('template/images/Default-welcomer.png')}}" alt="user-avatar" class="img-circle img-fluid">
       
    @else
    <img src="{{$admin->getFirstMediaUrl('images')}}" alt="user-avatar" class="img-circle img-fluid">
    
    @endif
                     
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>

                    @if(auth('web')->user()->id==$admin->id)
                    <a href="{{route('user.profile',['user'=>auth('web')->user()->token])}}" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> {{__('cms.view_profile')}}
                    </a>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            
          </div>
        </div>
        
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->


@endsection
 
 
 
 