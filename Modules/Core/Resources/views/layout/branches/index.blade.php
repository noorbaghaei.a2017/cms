@include('core::layout.modules.index',[

    'title'=>__('core::branches.index'),
    'items'=>$items,
    'parent'=>'core',
    'model'=>'branche',
    'class_model'=> 'branche',
    'directory'=>'branches',
    'collect'=>__('core::branches.collect'),
    'singular'=>__('core::branches.singular'),
    'create_route'=>['name'=>'branches.create'],
    'edit_route'=>['name'=>'branches.edit','name_param'=>'branch'],
    'destroy_route'=>['name'=>'branches.destroy','name_param'=>'branch'],
     'setting_route'=>true,
     'pagination'=>true,
    'datatable'=>[
        __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',

     __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
