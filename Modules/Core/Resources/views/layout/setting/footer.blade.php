@extends('core::layout.panel')
@section('pageTitle', __('cms.setting'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#">
                                @if(!$item->Hasmedia('footer'))
                                    <img style="width: 300px;height: auto" src="{{asset('img/no-img.gif')}}">

                                @else

                                    <img src="{{$item->getFirstMediaUrl('footer')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <small>
                            با استفاده از فرم زیر میتوانید تنظیمات پایین وب سایت را مشاهده کنید.
                        </small>
                        </div>
                        <div class="box-divider m-a-0"></div>
                        <div class="box-body">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                                @include('core::layout.alert-danger')
                            @if(\Illuminate\Support\Facades\Session::has('error'))
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{\Illuminate\Support\Facades\Session::get('error')}}</li>
                                    </ul>
                                </div>
                            @endif

                                <form id="signupForm" action="{{route('dashboard.footer.update')}}" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    {{method_field('PATCH')}}


                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                            @include('core::layout.load-single-image')
                                        </div>

                                    </div>




                                <div class="form-group row m-t-md">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.update')}} </button>
                                    </div>
                                </div>
                            </form>
                            <hr/>
                            <br/>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection





@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    name: {
                        required: true
                    },
                    domain: {
                        required: true
                    },
                    country: {
                        required: true
                    },
                    email: {
                        required: true
                    },
                    mobile: {
                        required: true
                    },
                    phone: {
                        required: true
                    },


                },
                messages: {
                    name:"عنوان الزامی است",
                    domain: "دامنه  لزامی است",
                    email: "ایمیل  الزامی است",
                    mobile: "موبایل  الزامی است",
                    phone: "تلفن  الزامی است",
                    country: "کشور الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
