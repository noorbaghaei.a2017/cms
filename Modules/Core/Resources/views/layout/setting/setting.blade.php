@extends('core::dashboard.main')

@section('content')
   



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.setting')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.setting')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">


                @if(!$item->Hasmedia('logo'))
                    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{asset('template/images/no-image.jpg')}}"
                       alt="" style="border-radius:unset;width:100%;">

    @else
    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{$item->getFirstMediaUrl('logo')}}"
                       alt="" style="border-radius:unset;width:100%;">

    @endif
                 


                </div>

                <h3 class="profile-username text-center">{{$item->title}}</h3>

                <p class="text-muted text-center">Manager</p>

               

               

             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.about-it')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                 
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">{{__('cms.setting')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#privacy" data-toggle="tab">{{__('cms.privacy_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#translate" data-toggle="tab">{{__('cms.translate')}}</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="tab-content">
                 
                 

                  <div class="tab-pane active" id="settings">
                    <form action="{{route('dashboard.setting.update')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PATCH')}}
                    <div class="form-group row">
                    <div class="col-sm-10">
                      <input name="image" type="file" onchange="loadFile(event)" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                    </div>  
                  
                      <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">{{__('cms.title')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="name" value="{{$item->name}}" class="form-control" id="name" placeholder="{{__('cms.name')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="slogan" class="col-sm-2 col-form-label">{{__('cms.slogan')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="slogan" value="{{$item->slogan}}" class="form-control" id="slogan" placeholder="{{__('cms.slogan')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="domain" class="col-sm-2 col-form-label">{{__('cms.domain')}}</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="{{$item->domain}}" name="domain" id="domain" placeholder="{{__('cms.domain')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="ios_app" class="col-sm-2 col-form-label">{{__('cms.ios_app')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="ios_app" value="{{$item->ios_app}}" class="form-control" id="ios_app" placeholder="{{__('cms.ios_app')}}">
                        </div>
                      </div>
                    
                      <div class="form-group row">
                        <label for="android_app" class="col-sm-2 col-form-label">{{__('cms.android_app')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="android_app" value="{{$item->android_app}}" class="form-control" id="android_app" placeholder="{{__('cms.android_app')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="mobile" class="col-sm-2 col-form-label">{{__('cms.mobile')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="mobile" value="{{$item->mobiles}}" class="form-control" id="mobile" placeholder="{{__('cms.mobile')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="fax" class="col-sm-2 col-form-label">{{__('cms.fax')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="fax" value="{{$item->faxs}}" class="form-control" id="fax" placeholder="{{__('cms.fax')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="phone" class="col-sm-2 col-form-label">{{__('cms.phone')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="phone" value="{{$item->phone}}" class="form-control" id="phone" placeholder="{{__('cms.phone')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label">{{__('cms.address')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="address" value="{{$item->address}}" class="form-control" id="address" placeholder="{{__('cms.address')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="map" class="col-sm-2 col-form-label">{{__('cms.map')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="map" value="{{$item->map}}" class="form-control" id="map" placeholder="{{__('cms.map')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">{{__('cms.email')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="email" value="{{$item->email}}" class="form-control" id="email" placeholder="{{__('cms.email')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="copy" class="col-sm-2 col-form-label">{{__('cms.copy-right')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="copy" value="{{$item->copy_right}}" class="form-control" id="copy" placeholder="{{__('cms.copy-right')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="title_seo" class="col-sm-2 col-form-label">{{__('cms.title_seo')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title_seo" value="{{$item->seo->title}}" class="form-control" id="title_seo" placeholder="{{__('cms.title_seo')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="description_seo" class="col-sm-2 col-form-label">{{__('cms.description_seo')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="description_seo" value="{{$item->seo->description}}" class="form-control" id="description_seo" placeholder="{{__('cms.description_seo')}}">
                        </div>
                      </div>
                   
                
                     
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="privacy">
                   
                  </div>


                  <div class="tab-pane" id="translate">
                
                      
                  <form action="{{route('setting.language.update',['lang'=>'fa','id'=>$item->id])}}" method="POST">
                  @csrf
                            @method('PATCH')

    
                     
                      <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">{{__('cms.name_fa')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="name" value="{{isset($item->translates->where('lang','fa')->first()->name) ? $item->translates->where('lang','fa')->first()->name : ''}}" class="form-control" id="name" placeholder="{{__('cms.name_fa')}}" autocomplete="off">
                        </div>
                      </div>
                           
                      <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">{{__('cms.title_fa')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title_seo" value="{{isset($item->translates->where('lang','fa')->first()->title_seo) ? $item->translates->where('lang','fa')->first()->title_seo : ''}}" class="form-control" id="name" placeholder="{{__('cms.title_fa')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">{{__('cms.description_fa')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="description_seo" value="{{isset($item->translates->where('lang','fa')->first()->description_seo) ? $item->translates->where('lang','fa')->first()->description_seo : ''}}" class="form-control" id="description" placeholder="{{__('cms.description_fa')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="copy_right" class="col-sm-2 col-form-label">{{__('cms.copy_right_fa')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="copy_right" value="{{isset($item->translates->where('lang','fa')->first()->copy_right) ? $item->translates->where('lang','fa')->first()->copy_right : ''}}" class="form-control" id="copy_right" placeholder="{{__('cms.copy_right_fa')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="slogan" class="col-sm-2 col-form-label">{{__('cms.solgen_fa')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="slogan" value="{{isset($item->translates->where('lang','fa')->first()->slogan) ? $item->translates->where('lang','fa')->first()->slogan : ''}}" class="form-control" id="slogan" placeholder="{{__('cms.slogen_fa')}}" autocomplete="off">
                        </div>
                      </div>



                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>

                      </form>
                      <form action="{{route('setting.language.update',['lang'=>'en','id'=>$item->id])}}" method="POST">
                  @csrf
                  @method('PATCH')
                  <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">{{__('cms.name_en')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="name" value="{{isset($item->translates->where('lang','en')->first()->name) ? $item->translates->where('lang','en')->first()->name : ''}}" class="form-control" id="name" placeholder="{{__('cms.name_en')}}" autocomplete="off">
                        </div>
                      </div>
                
                            <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">{{__('cms.title_en')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title_seo" value="{{isset($item->translates->where('lang','en')->first()->title_seo) ? $item->translates->where('lang','en')->first()->title_seo : ''}}" class="form-control" id="name" placeholder="{{__('cms.title')}}" autocomplete="off">
                        </div>
                      </div>
                    
                      <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">{{__('cms.description_en')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="description_seo" value="{{isset($item->translates->where('lang','en')->first()->description_seo) ? $item->translates->where('lang','en')->first()->description_seo : ''}}" class="form-control" id="description" placeholder="{{__('cms.description_en')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="copy_right" class="col-sm-2 col-form-label">{{__('cms.copy_right_en')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="copy_right" value="{{isset($item->translates->where('lang','en')->first()->copy_right) ? $item->translates->where('lang','en')->first()->copy_right : ''}}" class="form-control" id="copy_right" placeholder="{{__('cms.copy_right_en')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="slogan" class="col-sm-2 col-form-label">{{__('cms.slogan_en')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="slogan" value="{{isset($item->translates->where('lang','en')->first()->slogan) ? $item->translates->where('lang','en')->first()->slogan : ''}}" class="form-control" id="slogan" placeholder="{{__('cms.slogan_en')}}" autocomplete="off">
                        </div>
                      </div>
                    
                      


                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>

                      </form>

                   
                    

                   </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>





@endsection



@section('scripts')


<script>

function loadFile(e) {
	 image = $('#show-image');
    
    image.attr('src',URL.createObjectURL(event.target.files[0]));
    
};


</script>

@endsection
 
 
 
