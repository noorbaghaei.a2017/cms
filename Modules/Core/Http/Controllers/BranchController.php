<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Branch;
use Modules\Core\Http\Requests\BranchRequest;

class BranchController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Branch();

        
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('core::layout.branches.index',compact('items'));
        }catch (\Exception $exception){
          
            return abort('500');
        }


    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $items=Branch::latest()->get();
            return view('core::layout.branches.create',compact('items'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(BranchRequest $request)
    {
        try {
           

            $saved=Branch::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'text'=>$request->input('text'),
                'excerpt'=>$request->input('excerpt'),
                'address'=>$request->input('address'),
                'google_map'=>$request->input('google_map'),
                'latitude'=>$request->input('latitude'),
                'longitude'=>$request->input('longitude'),
             
                'token'=>tokenGenerate(),
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('core::branches.error'));
            }else{
                return redirect(route('branches.index'))->with('message',__('core::branches.store'));
            }

        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

 

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {

            $item=Branch::whereToken($token)->first();

            return view('core::layout.branches.edit',compact('item'));

        }catch (\Exception $exception){
           return dd($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(BranchRequest $request, $token)
    {
        try {


         
            $item=Branch::whereToken($token)->first();
            $updated=$item->update([
                'title'=>$request->input('title'),
                "slug"=>null,
                'symbol'=>$request->input('symbol'),
                'text'=>$request->input('text'),
                'excerpt'=>$request->input('excerpt'),
                'address'=>$request->input('address'),
                'google_map'=>$request->input('google_map'),
                'latitude'=>$request->input('latitude'),
                'longitude'=>$request->input('longitude'),

                
            ]);

            $item->replicate();

            if(!$updated){
                return redirect()->back()->with('error',__('core.branches.error'));
            }else{
                return redirect(route('branches.index'))->with('message',__('core::branches.store'));
            }

        }catch (\Exception $exception){
          
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        //
    }
}
