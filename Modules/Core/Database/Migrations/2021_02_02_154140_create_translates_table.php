<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lang');
            $table->string('title')->nullable();
            $table->string('excerpt')->nullable();
            $table->string('symbol')->nullable();
            $table->string('name')->nullable();
            $table->text('copy_right')->nullable();
            $table->text('address')->nullable();
            $table->text('slogan')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->string('answer')->unique()->nullable();
            $table->string('title_seo')->nullable();
            $table->string('description_seo')->nullable();
            $table->string('canonical_seo')->nullable();
            $table->string('keyword_seo')->nullable();
            $table->text('text')->nullable();
            $table->morphs('translateable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translates');
    }
}
